'use strict';

/**
 * @ngdoc overview
 * @name clientApp
 * @description
 * # clientApp
 *
 * Main module of the application.
 */
angular
  .module('betting', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
	'ui.router',
    'ngSanitize',
    'ngTouch',
    'features',
    'ngMaterial',
    'common'
  ])
	.factory('authHttpResponseInterceptor',['$q','$location','$rootScope',function($q,$location,$rootScope){

        return {
            response: function(response){
                $rootScope.loading = false;
                return response ;
            },
			responseError: function(rejection) {
				if (rejection.status === 401) {
					$location.path('/account/login');
				}
				return $q.reject(rejection);
			}
		}
	}])
    .factory('routingGlobal',[function(){
        return {
            prefix : 'http://localhost:9000'//'http://52.33.46.247/servers'//
        };
    }])
  .config(['$stateProvider',function ($stateProvider) {
	 $stateProvider
     .state('index',{
         url : '',
         templateUrl: 'modules/features/account/login/views/accountLogin.html',
         data: { requiresLogin: false }
     })
    .state('account_login',{
				url : '/account/login',
        templateUrl: 'modules/features/account/login/views/accountLogin.html',
			 	data: { requiresLogin: false }
    })
    .state('account_logout',{
     url : '/account/logout',
     templateUrl: 'modules/features/account/logout/views/accountLogout.html',
     data: { requiresLogin: true }
    })
    .state('account_edit',{
        url : '/account/edit',
        templateUrl: 'modules/features/account/edit/views/accountEdit.html',
        data: { requiresLogin: true}
    })
    .state('account_register',{
        url : '/account/register',
        templateUrl: 'modules/features/account/register/views/accountRegister.html',
        data: { requiresLogin: false }
    })
    .state('account_activate',{
        url : '/account/activate/{uid}/{hash}',
        templateUrl: 'modules/features/account/register/views/accountRegister.html',
        data: { requiresLogin: true }
    })
    .state('events_between_dates',{
        url : '/events/between_dates',
        templateUrl: 'modules/features/events/betweenDates/views/eventsBetweenDates.html',
        data: { requiresLogin: true }
    })
    .state('events_by_leagues',{
        url : '/events/by_leagues/{start}/{end}',
        templateUrl: 'modules/features/events/byLeagues/views/eventsByLeagues.html',
        data: { requiresLogin: true },
        params: {
          start: {value: null, squash: true},
          end: {value: null, squash: true}
        }
    })
    .state('rooms_game_room',{
        url : '/rooms/game_room/{roomID}',
        templateUrl: 'modules/features/rooms/gameRoom/views/gameRoom.html',
        data: { requiresLogin: true },
        params: { room:null, roomID:null }
    })
    .state('event_details',{
        url : '/events/event_details/{eventID}',
        templateUrl: 'modules/features/events/eventDetails/views/eventDetails.html',
        data: { requiresLogin: true },
        params: { event:null, eventID:null }
    })
  }])
	.config(['$httpProvider',function($httpProvider) {
		//Http Intercpetor to check auth failures for xhr requests
		$httpProvider.interceptors.push('authHttpResponseInterceptor');
	}])
    .config(['$mdDateLocaleProvider',function($mdDateLocaleProvider) {

    $mdDateLocaleProvider.parseDate = function(dateString) {
      return moment(dateString).toDate();
    };
    $mdDateLocaleProvider.formatDate = function(date) {
      return moment(date).format('YYYY-MM-DD');
    };

  }])
    .run(['$rootScope', '$state','accountService',function ($rootScope, $state,accountService) {

        $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
            var requireLogin = toState.data.requiresLogin;

            if (requireLogin && !accountService.user.login) {
                event.preventDefault();
                accountService.isLogin().then(function(){
                    accountService.user.login = true;
                    $state.go(toState,toParams);
                });
            }
        });
    }]);
