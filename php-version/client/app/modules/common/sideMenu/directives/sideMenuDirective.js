/**
 * Created by workmain on 02/09/15.
 */
angular.module('common').directive('sideMenu',['$state','$rootScope', '$mdSidenav', function($state,$rootScope, $mdSidenav) {
    return {
      restrict: 'E',
      replace:true,
      link: function(scope, element, attrs) {
          scope.close = function () {
              $mdSidenav('left').close()
                  .then(function () {
                      return ;
                  });
          };
      },
      templateUrl: 'modules/common/sideMenu/views/sideMenu.html'
    };
}]);
