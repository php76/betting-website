/**
 * Created by workmain on 04/09/15.
 */
angular.module('common').directive('toggleMenu',[ function() {
    return {
      restrict: 'E',
      scope: {
        toggleHeader : "="
      },
      transclude: true,
      link: function(scope, element, attrs) {
        scope.collapse = true;
      },
      templateUrl: 'modules/common/toggleMenu/views/toggleMenu.html'
    };
}]);
