/**
 * Created by workmain on 25/07/15.
 */
angular.module('common').directive('header',['$state','$rootScope', '$mdSidenav', '$mdUtil', function($state,$rootScope, $mdSidenav, $mdUtil) {
    return {
      restrict: 'E',
      link: function(scope, element, attrs) {
        $rootScope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){
            scope.pageTitle = $state.current.name;
        });

          scope.toggleLeft = buildToggler('left');

          function buildToggler(navID) {
              var debounceFn =  $mdUtil.debounce(function(){
                  $mdSidenav(navID).toggle();
                      /*.then(function () {
                          console.log('aas');
                      });*/
              },200);
              return debounceFn;
          }

      },
      templateUrl: 'modules/common/header/views/header.html'
    };
}]);
