/**
 * Created by cohendo on 17/11/2015.
 */

angular.module('rooms').controller('roomsCreateRoomCtrl',['$scope','roomsService','$location','$mdDialog',function($scope,roomsService,$location,$mdDialog){
    'use strict';

    $scope.types = [ "type1","type2","type3" ];


    $scope.cancel = function() {
      $mdDialog.cancel();
    };

}]);
