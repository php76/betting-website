/**
 * Created by cohendo on 26/09/2015.
 */

angular.module('rooms').controller('gameRoomCtrl',['$scope','roomsService','$stateParams','$timeout','$interval',function($scope,roomsService,$stateParams,$timeout,$interval){
    'use strict';

    if($stateParams.event) {
      $scope.event = $stateParams.event;
    }else{
      roomsService.getByRoomId($stateParams.roomID).then(function(roomData){
        $scope.room = roomData.data.room;
      });
    };

    $scope.stopChat = false;

    $scope.sendChatMessage = function(){
      roomsService.sendChatMessage($stateParams.roomID,$scope.message).then(function(response){
        $scope.readChatMessages();
        $scope.message = null;
      });
    } ;

    angular.element(document.querySelector('#chatroom .md-virtual-repeat-scroller')).on('$destroy', function() {
      $interval.cancel($scope.promise);
    });

    $scope.readChatMessages = function(){
      roomsService.readChatMessages($stateParams.roomID).then(function(chatData){
        $scope.chat = chatData.data.chat;
        $timeout(function(){
          var chatroom = document.querySelector('#chatroom .md-virtual-repeat-scroller');
          chatroom.scrollTop=chatroom.scrollHeight;
        }, 0);
      });
    };

    $scope.changeChatStatus = function(){
      $scope.promise = $scope.stopChat?$interval.cancel($scope.promise):$interval($scope.readChatMessages, 2500);
      $scope.stopChat = $scope.stopChat?false:true;
      $scope.chatStatusText = $scope.stopChat?"Stop Chat":"Resume Chat";
      $scope.chatStatusIcon = $scope.stopChat?"stop":"play_arrow";
    };

    $scope.changeChatStatus();

}]);
