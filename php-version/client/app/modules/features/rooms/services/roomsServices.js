/**
 * Created by cohendo on 26/09/2015.
 */
angular.module('rooms').service('roomsService',['$http','routingGlobal',function($http,routingGlobal) {
    'use strict';

    this.getByRoomId = function(data){
      return $http.post(routingGlobal.prefix+'/api/soccer/rooms/get_by_room_id',angular.toJson({room_id:data}));
    };

    this.sendChatMessage = function(roomID,message){
      return $http.post(routingGlobal.prefix+'/api/soccer/rooms/chat/send_message',angular.toJson({room_id:roomID,message:message}));
    };

    this.readChatMessages = function(roomID){
      return $http.post(routingGlobal.prefix+'/api/soccer/rooms/chat/read_messages',angular.toJson({room_id:roomID}));
    };

}]);
