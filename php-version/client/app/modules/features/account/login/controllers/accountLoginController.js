/**
 * Created by cohendo on 21/07/2015.
 */

angular.module('account').controller('accountLoginCtrl',['$scope','accountService','$state','$mdDialog',function($scope,accountService,$state,$mdDialog){
    'use strict';

		accountService.user.login = false;

    $scope.login = function(account) {
      accountService.login(account).success(function(){
				$state.go('account_edit');
			});
    };

    $scope.register = function() {
			$state.go("account_register");
    };

    $scope.fbLogin = function() {
      accountService.logout();
    };

    $scope.forgotPassword = function(event) {
      $mdDialog.show({
        templateUrl: 'modules/features/account/forgotPassword/views/accountForgotPassword.html',
        parent: angular.element(document.body),
        targetEvent: event
      });
    };

}]);
