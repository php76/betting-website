/**
 * Created by cohendo on 21/07/2015.
 */

angular.module('account').controller('accountEditCtrl',['$scope','accountService','$filter',function($scope,accountService,$filter){
    'use strict';


    accountService.getUser().then(function(userData){
        userData.data.user.userBirthDate = new Date($filter('date')(new Date(userData.data.user.userBirthDate), 'yyyy-MM-dd'));
        $scope.account = userData.data.user;
    });

    $scope.update = function(user) {
        accountService.register(user).then(function(){
        });
    };

    $scope.states = ('AL AK AZ AR CA CO CT DE FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS ' +
    'MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI ' +
    'WY').split(' ').map(function (state) { return { abbrev: state }; });

}]);
