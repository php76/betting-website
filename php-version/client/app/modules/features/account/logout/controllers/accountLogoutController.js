/**
 * Created by cohendo on 21/07/2015.
 */

angular.module('account').controller('accountLogoutCtrl',['$scope','accountService','$state',function($scope,accountService,$state){
    'use strict';

    accountService.logout().then(function(){
      $state.go('account_login');
    });

}]);
