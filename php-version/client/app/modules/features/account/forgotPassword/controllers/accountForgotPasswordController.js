/**
 * Created by cohendo on 26/07/2015.
 */

angular.module('account').controller('accountForgotPasswordCtrl',['$scope','accountService','$location','$mdDialog',function($scope,accountService,$location,$mdDialog){
    'use strict';

    $scope.update = function(user) {
        accountService.login(user).then(function(){

        });
    };

    $scope.register = function() {
      $location.path('/account/register');
    };

    $scope.cancel = function() {
      $mdDialog.cancel();
    };

}]);
