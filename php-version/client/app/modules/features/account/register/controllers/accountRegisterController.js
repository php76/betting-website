/**
 * Created by cohendo on 21/07/2015.
 */

angular.module('account').controller('accountRegisterCtrl',['$scope','accountService',function($scope,accountService){
    'use strict';

    $scope.update = function(user) {
        accountService.register(user).then(function(){

        });
    };
    $scope.states = ('AL AK AZ AR CA CO CT DE FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS ' +
    'MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI ' +
    'WY').split(' ').map(function (state) { return { abbrev: state }; });

}]);
