/**
 * Created by cohendo on 22/07/2015.
 */
angular.module('account').service('accountService',['$http','routingGlobal','$rootScope',function($http,routingGlobal,$rootScope) {
    'use strict';

    this.user = {
			login : false,
			details : null
		}

    this.login = function(data){
        $rootScope.loading = true;
        return $http.post(routingGlobal.prefix+'/api/account/login',angular.toJson({"account_login":data}));
    }

    this.edit = function(data){
        $rootScope.loading = true;
        return $http.post(routingGlobal.prefix+'/api/account/edit',angular.toJson({"account_edit":data}));
    }

    this.register = function(data){
        return $http.post(routingGlobal.prefix+'/api/account/register',angular.toJson({"accoun_register":data}));
    }

    this.getUser = function(){
        $rootScope.loading = true;
        return $http.post(routingGlobal.prefix+'/api/account/get_user');
    }

    this.isLogin = function(){
        return $http.post(routingGlobal.prefix+'/api/account/is_login');
    }

    this.logout = function(){
      return $http.post(routingGlobal.prefix+'/api/account/logout');
    }

}]);
