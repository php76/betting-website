/**
 * Created by cohendo on 01/08/2015.
 */
angular.module('events').service('eventsService',['$http','routingGlobal','$rootScope',function($http,routingGlobal,$rootScope) {
    'use strict';

    this.getBetweenDates = function(data){
        return $http.post(routingGlobal.prefix+'/api/soccer/events/get_between_dates',angular.toJson({"events_between_dates_soccer":data}));
    };

    this.getByLeagues = function(data){
        $rootScope.loading = true;
        return $http.post(routingGlobal.prefix+'/api/soccer/events/get_by_leagues',angular.toJson({"events_between_dates_soccer":data}));
    };

    this.getByEventId = function(data){
        $rootScope.loading = true;
        return $http.post(routingGlobal.prefix+'/api/soccer/events/get_by_event_id',angular.toJson({event_id:data}));
    };

    this.getByDistance = function(data){
        return $http.post(routingGlobal.prefix+'/api/soccer/events/get_by_distance',angular.toJson({"events_between_dates_soccer":data}));
    };

}]);
