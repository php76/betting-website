/**
 * Created by cohendo on 01/08/2015.
 */

angular.module('events').controller('eventsByLeaguesCtrl',['$scope','eventsService','$state','$stateParams','$timeout', function($scope,eventsService,$state,$stateParams,$timeout){
    'use strict';
  $scope.leagues=null;

  $scope.startDate = moment($stateParams.start).isValid() ? moment($stateParams.start).toDate() : moment().toDate();
  $scope.endDate   = moment($stateParams.end).isValid()   ? moment($stateParams.end).toDate()   : moment().add(7,'days').toDate();

  $scope.eventDetails = function(event){
    $state.go('event_details',{event : event, eventID : event.id});
  };

  $scope.getBetweenDates = function(){
    if(moment($scope.startDate).isValid() && moment($scope.endDate).isValid()){
      var dates ={ from_date : moment($scope.startDate).format("YYYY-MM-DD"), to_date : moment($scope.endDate).format("YYYY-MM-DD") };
      eventsService.getByLeagues(dates).then(function(eventsData){
        $scope.leagues = eventsData.data.leagues;
      });
    }
  };

  $scope.changeDates = function(){
      $state.transitionTo('events_by_leagues', {start : moment($scope.startDate).format("YYYY-MM-DD"), end : moment($scope.endDate).format("YYYY-MM-DD")}, { location: false,inherit: true, relative: $state.$current, notify: true });
  };
  $timeout($scope.getBetweenDates,0);

}]);
