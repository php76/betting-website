/**
 * Created by cohendo on 02/09/2015.
 */

angular.module('events').controller('eventDetailsCtrl',['$scope','eventsService','$stateParams','$mdDialog',function($scope,eventsService,$stateParams,$mdDialog){
    'use strict';

    if($stateParams.event) {
      $scope.event = $stateParams.event;
    }else{
      eventsService.getByEventId($stateParams.eventID).then(function(eventData){
        $scope.event = eventData.data.event;
      });
    }

    $scope.createRoom = function(event) {
        $mdDialog.show({
            templateUrl: 'modules/features/rooms/createRoom/views/roomsCreateRoom.html',
            parent: angular.element(document.body),
            targetEvent: event
        });
    };

}]);
