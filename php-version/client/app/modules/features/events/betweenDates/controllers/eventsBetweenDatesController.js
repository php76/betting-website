/**
 * Created by cohendo on 01/08/2015.
 */

angular.module('events').controller('eventsBetweenDatesCtrl',['$scope','eventsService',function($scope,eventsService){
    'use strict';
  $scope.events=null;

  var dates ={ from_date : "2015-07-27", to_date : "2015-07-28" };

  eventsService.getBetweenDates(dates).then(function(eventsData){
    $scope.events = eventsData.data.events;
  });


}]);
