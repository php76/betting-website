<?php

namespace Betting\Servers\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Betting\Servers\ApplicationBundle\Entity\Users;
use Betting\Servers\ApplicationBundle\Form\Account\RegistrationType;
use Betting\Servers\ApplicationBundle\Form\Account\EditType;
use Betting\Servers\ApplicationBundle\Form\Account\LoginType;
use Betting\Servers\ApplicationBundle\Form\Account\ChangePasswordType;
use Betting\Servers\ApplicationBundle\Form\Account\ResetPasswordType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View;


use Symfony\Component\HttpKernel\Exception\HttpNotFoundException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;


class AccountController extends FOSRestController
{
    protected function loginUser(Users $user){
        $security = $this->get('security.context');
        $providerKey = 'secured_area';
        $roles = $user->getRoles();
        $token = new UsernamePasswordToken($user, null, $providerKey, $roles);
        $security->setToken($token);
        $request = $this->get('request');
        $event = new InteractiveLoginEvent($request, $token);
        return $this->get("event_dispatcher")->dispatch("security.interactive_login",$event);
    }

    protected function logoutUser(){
        $security = $this->get('security.context');
        $token = new AnonymousToken(null, new Users());
        $security->setToken($token);
        $this->get('session')->invalidate();
    }

    protected function checkUserPassword(Users $user, $password){
        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);
        if(!$encoder){
            return false;
        }

        return $encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt());
    }

    /**
     * Register a new user
     * route name: Register
     * pattern: /api/account/register.{_format}
     * http method requirement: POST
     *
     * @Annotations\Post("/register")
     * @Annotations\View()
     */
    public function registerAction(Request $request){
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new RegistrationType(), new Users());

        $form->handleRequest($request);

        if ($form->isValid()) {
            $registration = $form->getData();

            $em->persist($registration);
            $em->flush();

            $message = \Swift_Message::newInstance()
                ->setSubject('Verify your account')
                ->setFrom('verify@betme.com')
                ->setTo($registration->getUserEmail())
                ->setBody(
                    $this->renderView(
                        'BettingServersApplicationBundle:Account:verifyEmail.html.twig',
                        array( 'name' => $registration->getUserName(),'link' => $registration->getUserHash() )
                    ),
                    'text/html' // 'text/plain'
                );

            $this->get('mailer')->send($message);

            throw new HttpException(200, "Success");

        }elseif($form->isSubmitted()){
            return array('errors' => $form->getErrorsAsString());
        }
        throw new HttpException(400, "Bad Request");
    }

    /**
     * login user and set session
     * route name: login
     * pattern: /api/account/login.{_format}
     * http method requirement: POST
     *
     * @Annotations\Post("/login")
     * @Annotations\View()
     */
    public function loginAction(Request $request){

        $form = $this->createForm(new LoginType(), new Users(),array('action'   =>'api/account/login_check'));
        $form->handleRequest($request);

        if($form->isValid()){
            $password = $form->get('password')->getData();
            $username = $form->get('userName')->getData();

            $em = $this->getDoctrine()->getEntityManager();
            $user = $em->getRepository('BettingServersApplicationBundle:Users')->findOneByUserName($username);

            if(!$user){
                $user = $em->getRepository('BettingServersApplicationBundle:Users')->findOneByEmail($username);
            }

            if(!$user instanceof Users){
                throw new HttpException(401, "User Not Exist");
            } elseif(!$this->checkUserPassword($user, $password)){
                throw new HttpException(401, "User Wrong Password");
            }else{
                $this->loginUser($user);
                throw new HttpException(200, "Success");
            }
        }elseif($form->isSubmitted()){
            return array('errors' => $form->getErrorsAsString());
        }
        throw new HttpException(400, "Bad Request");
    }

    /**
     * Logout user and clear session
     * route name: logout
     * pattern: /api/account/logout.{_format}
     * http method requirement: POST
     *
     * @Annotations\Post("/logout")
     * @Annotations\View()
     */
    public function logoutAction(){
        $this->logoutUser();
        throw new HttpException(200, "Success");
    }


    /**
     * Change Password Form
     * route name: change_password
     * pattern: /api/account/change_password/{$userId}/{$hash}.{_format}
     * http method requirement: POST
     *
     * @Annotations\Post("/change_password/{$userId}/{$hash}")
     * @Annotations\View()
     */
    public function changePasswordAction($userId,$hash,Request $request){

        $form = $this->createForm(new ChangePasswordType(), new Users());

        $em = $this->getDoctrine()->getEntityManager();
        $user = $em->getRepository('BettingServersApplicationBundle:Users')->findOneById((int)$userId);

        if($user && $user->getUserHash() === $hash){
            $form->handleRequest($request);

            if($form->isValid()){
                $password = $form->get('userPassword')->getData();
                $user->setUserPassword($password);
                $user->setUserHash($user->generateHash());
                $em->persist($user);
                $em->flush();
                throw new HttpException(200, "Success");
            }elseif($form->isSubmitted()){
                return array('errors' => $form->getErrorsAsString());
            }
        }
        throw new HttpException(400, "Bad Request");
    }

    /**
     * Reset user password by an email been sent to his address
     * route name: reset_password
     * pattern: /api/account/reset_password.{_format}
     * http method requirement: POST
     *
     * @Annotations\Post("/reset_password")
     * @Annotations\View()
     */
    public function resetPasswordAction(Request $request){
        $form = $this->createForm(new ResetPasswordType(), new Users());

        $form->handleRequest($request);
        if ($form->isValid()) {
            $data = $form->getData();

            $em = $this->getDoctrine()->getEntityManager();
            $user = $em->getRepository('BettingServersApplicationBundle:Users')->findOneByUserEmail($data->getUserEmail());
            $message = \Swift_Message::newInstance()
                ->setSubject('Verify your account')
                ->setFrom('verify@betme.com')
                ->setTo($user->getUserEmail())
                ->setBody(
                    $this->renderView(
                        'BettingServersApplicationBundle:Account:verifyEmail.html.twig',
                        array( 'name' => $user->getUserName(),'link' => $user->getUserHash() )
                    ),
                    'text/html' // 'text/plain'
                );

            $this->get('mailer')->send($message);
            throw new HttpException(200, "Success");
        }elseif($form->isSubmitted()){
            return array('errors' => $form->getErrorsAsString());
        }
        throw new HttpException(400, "Bad Request");
    }

    /**
     * Edit user account Details
     * route name: edit
     * pattern: /api/account/edit.{_format}
     * http method requirement: POST
     *
     * @Annotations\Post("/edit")
     * @Annotations\View()
     */
    public function editAction(Request $request){

        $em = $this->getDoctrine()->getEntityManager();
        $userProfile = $this->get('security.context')->getToken()->getUser();

        $form = $this->createForm(new EditType(), $userProfile);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $registration = $form->getData();
            $em->persist($registration);
            $em->flush();
            throw new HttpException(200, "Success");

        }elseif($form->isSubmitted()){
            return array('errors' => $form->getErrorsAsString());
        }
        throw new HttpException(400, "Bad Request");
    }

    /**
     * Activate user account
     * route name: activate
     * pattern: /api/account/activate/{userId}/{hash}.{_format}
     * http method requirement: POST
     *
     * @Annotations\Post("/activate/{userId}/{hash}")
     * @Annotations\View()
     */
    public function activateAction($userId,$hash){
        $em = $this->getDoctrine()->getEntityManager();
        $user = $em->getRepository('BettingServersApplicationBundle:Users')->findOneById((int)$userId);
        if($user && $user->getUserHash() === $hash){
            $user->setUserEnabled(true);
            $user->setUserHash($user->generateHash());
            $em->persist($user);
            $em->flush();
            throw new HttpException(200, "Success") ;
        }else{
            throw new HttpException(400, "Bad Request");
        }
    }

    /**
     * Get user account Details
     * route name: get_user
     * pattern: /api/account/get_user.{_format}
     * http method requirement: POST
     *
     * @Annotations\Post("/get_user")
     * @Annotations\View()
     */
    public function getUserAction(){
        $user = $this->get('security.context')->getToken()->getUser();
        return array('user' => $user);
    }

    /**
     * Check if user is login
     * route name: is_login
     * pattern: /api/account/is_login.{_format}
     * http method requirement: POST
     *
     * @Annotations\Post("/is_login")
     * @Annotations\View()
     */
    public function isLoginAction(){
        throw new HttpException(200, "Success") ;
    }

}
