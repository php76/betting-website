<?php

namespace Betting\Servers\ApplicationBundle\Controller\Soccer;


use Betting\Servers\ApplicationBundle\Form\RegistrationType;
use Betting\Servers\ApplicationBundle\Form\EditType;
use Betting\Servers\ApplicationBundle\Form\Events\Soccer\EventsBetweenDatesType;
use Betting\Servers\EventsBundle\Form\EventsType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View;


use Symfony\Component\HttpKernel\Exception\HttpNotFoundException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;

class EventsController extends FOSRestController
{

    /**
     * get events by Leagues grouping
     * route name: get_by_leagues
     * pattern: /api/soccer/events/get_by_leagues.{_format}
     * http method requirement: POST
     *
     * @Annotations\Post("/get_by_leagues")
     * @Annotations\View()
     */
    public function getByLeaguesAction(Request $request)
    {
        $form = $this->createForm(new EventsBetweenDatesType());
        $form->handleRequest($request);

        if ($form->isValid()) {

            $fromDate = $form->get('fromDate')->getData();
            $toDate = $form->get('toDate')->getData();

            $em = $this->getDoctrine()->getEntityManager();
            $leagues = $em->getRepository('BettingServersEventsBundle:Soccer\Leagues')->findBetweenDates($fromDate, $toDate);

            return array('leagues' => $leagues);
        }elseif($form->isSubmitted()){
            return array('errors' => $form->getErrors());
        }
        throw new HttpException(400, "Bad Request");
    }

    /**
     * get events by id
     * route name: get_by_event_id
     * pattern: /api/soccer/events/get_by_event_id.{_format}
     * http method requirement: POST
     *
     * @Annotations\Post("/get_by_event_id")
     * @Annotations\View()
     */
    public function getByEventIdAction(Request $request)
    {
        $eventID = $request->request->get('eventId');

        if (isset($eventID)) {
            $em = $this->getDoctrine()->getEntityManager();
            $event = $em->getRepository('BettingServersEventsBundle:Soccer\Events')->findOneById($eventID);

            return array('event' => $event);
        }

        throw new HttpException(400, "Bad Request");
    }

}
