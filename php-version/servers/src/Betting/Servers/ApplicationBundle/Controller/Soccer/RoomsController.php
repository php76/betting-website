<?php

namespace Betting\Servers\ApplicationBundle\Controller\Soccer;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Betting\Servers\ApplicationBundle\Entity\Users;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View;


use Symfony\Component\HttpKernel\Exception\HttpNotFoundException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;


class RoomsController extends FOSRestController
{

    /**
     * create a new room
     * route name: create_room
     * pattern: /api/soccer/rooms/create_room.{_format}
     * http method requirement: POST
     *
     * @Annotations\Post("/create_room")
     * @Annotations\View()
     */
    public function createRoomAction(Request $request)
    {
        $eventID = $request->request->get('eventId');

        if (isset($eventID)) {
            $em = $this->getDoctrine()->getEntityManager();
            $event = $em->getRepository('BettingServersEventsBundle:Soccer\Events')->findOneById($eventID);

            return array('event' => $event);
            return array('status'=>'success');
        }

        throw new HttpException(400, "Bad Request");
    }

    /**
     * get details by room id
     * route name: get_by_room_id
     * pattern: /api/soccer/rooms/get_by_room_id.{_format}
     * http method requirement: POST
     *
     * @Annotations\Post("/get_by_room_id")
     * @Annotations\View()
     */
    public function getByRoomIdAction(Request $request)
    {
        $roomID = $request->request->get('roomId');

        if (isset($roomID)) {
            $em = $this->getDoctrine()->getEntityManager();
            $room = $em->getRepository('BettingServersApplicationBundle:Soccer\Rooms')->findOneById($roomID);

            return array('room' => $room);
        }

        throw new HttpException(400, "Bad Request");
    }

    /**
     * read a chat file
     * route name: read_messages
     * pattern: /api/soccer/rooms/chat/read_messages.{_format}
     * http method requirement: POST
     *
     * @Annotations\Post("/chat/read_messages")
     * @Annotations\View()
     */
    public function chatReadMessagesAction(Request $request)
    {
        $roomID  = $request->request->get('roomId');
        $message = $request->request->get('message');
        $fileName = "chatfiles/".$roomID.".csv";

        if (file_exists($fileName)){
            $handle = fopen($fileName, "r");
            while ($data = fgetcsv($handle, 1024)) {
                $chat[] = array('date'=>$data[0],'user'=>$data[1],'message'=>$data[2]);
            }
        }

        return array('chat'=>$chat);
    }

    /**
     * send a chat message
     * route name: send_message
     * pattern: /api/soccer/rooms/chat/send_message.{_format}
     * http method requirement: POST
     *
     * @Annotations\Post("/chat/send_message")
     * @Annotations\View()
     */
    public function chatSendMessageAction(Request $request)
    {
        $roomID  = $request->request->get('roomId');
        $message = $request->request->get('message');
        $fileName = "chatfiles/".$roomID.".csv";
        $folderName = "chatFiles";


        if (!file_exists($folderName)) {
            mkdir($folderName, 0644);
        }

        if (file_exists($fileName)){
            $handle = fopen($fileName, "a");
        }else{
            $handle = fopen($fileName, "w");
        }

        fputcsv($handle, array(
            'date'=>date('m/d/Y h:i:s a', time()),
            'user'=>$this->get('security.context')->getToken()->getUser()->getUserName(),
            'message'=>$message
        ));
        fclose($handle);

        return array('status'=>'success');
    }

}
