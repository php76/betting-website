<?php

namespace Betting\Servers\ApplicationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Friendship
 *
 * @ORM\Table(name="app_friendship")
 * @ORM\Entity(repositoryClass="Betting\Servers\ApplicationBundle\Entity\FriendshipRepository")
 */
class Friendship
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user", type="integer")
     */
    private $user;

    /**
     * @var integer
     *
     * @ORM\Column(name="friend", type="integer")
     */
    private $friend;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestamp", type="datetime")
     */
    private $timestamp;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="userWins", type="integer")
     */
    private $userWins;

    /**
     * @var integer
     *
     * @ORM\Column(name="friendWins", type="integer")
     */
    private $friendWins;

    /**
     * @var string
     *
     * @ORM\Column(name="friendshipRank", type="string", length=255)
     */
    private $friendshipRank;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param integer $user
     * @return Friendship
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return integer 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set friend
     *
     * @param integer $friend
     * @return Friendship
     */
    public function setFriend($friend)
    {
        $this->friend = $friend;

        return $this;
    }

    /**
     * Get friend
     *
     * @return integer 
     */
    public function getFriend()
    {
        return $this->friend;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return Friendship
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Friendship
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set userWins
     *
     * @param integer $userWins
     * @return Friendship
     */
    public function setUserWins($userWins)
    {
        $this->userWins = $userWins;

        return $this;
    }

    /**
     * Get userWins
     *
     * @return integer 
     */
    public function getUserWins()
    {
        return $this->userWins;
    }

    /**
     * Set friendWins
     *
     * @param integer $friendWins
     * @return Friendship
     */
    public function setFriendWins($friendWins)
    {
        $this->friendWins = $friendWins;

        return $this;
    }

    /**
     * Get friendWins
     *
     * @return integer 
     */
    public function getFriendWins()
    {
        return $this->friendWins;
    }

    /**
     * Set friendshipRank
     *
     * @param string $friendshipRank
     * @return Friendship
     */
    public function setFriendshipRank($friendshipRank)
    {
        $this->friendshipRank = $friendshipRank;

        return $this;
    }

    /**
     * Get friendshipRank
     *
     * @return string 
     */
    public function getFriendshipRank()
    {
        return $this->friendshipRank;
    }
}
