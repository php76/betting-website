<?php

namespace Betting\Servers\ApplicationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transactions
 *
 * @ORM\Table(name="app_transactions")
 * @ORM\Entity(repositoryClass="Betting\Servers\ApplicationBundle\Entity\TransactionsRepository")
 */
class Transactions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestamp", type="datetime")
     */
    private $timestamp;

    /**
     * @var string
     *
     * @ORM\Column(name="payment", type="string", length=255)
     */
    private $payment;

    /**
     * @var integer
     *
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;

    /**
     * @var boolean
     *
     * @ORM\Column(name="withdraw", type="boolean")
     */
    private $withdraw;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deposit", type="boolean")
     */
    private $deposit;

    /**
     * @var integer
     *
     * @ORM\Column(name="balanceBefore", type="integer")
     */
    private $balanceBefore;

    /**
     * @var integer
     *
     * @ORM\Column(name="balanceAfter", type="integer")
     */
    private $balanceAfter;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return Transactions
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set payment
     *
     * @param string $payment
     * @return Transactions
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return string 
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return Transactions
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set withdraw
     *
     * @param boolean $withdraw
     * @return Transactions
     */
    public function setWithdraw($withdraw)
    {
        $this->withdraw = $withdraw;

        return $this;
    }

    /**
     * Get withdraw
     *
     * @return boolean 
     */
    public function getWithdraw()
    {
        return $this->withdraw;
    }

    /**
     * Set deposit
     *
     * @param boolean $deposit
     * @return Transactions
     */
    public function setDeposit($deposit)
    {
        $this->deposit = $deposit;

        return $this;
    }

    /**
     * Get deposit
     *
     * @return boolean 
     */
    public function getDeposit()
    {
        return $this->deposit;
    }

    /**
     * Set balanceBefore
     *
     * @param integer $balanceBefore
     * @return Transactions
     */
    public function setBalanceBefore($balanceBefore)
    {
        $this->balanceBefore = $balanceBefore;

        return $this;
    }

    /**
     * Get balanceBefore
     *
     * @return integer 
     */
    public function getBalanceBefore()
    {
        return $this->balanceBefore;
    }

    /**
     * Set balanceAfter
     *
     * @param integer $balanceAfter
     * @return Transactions
     */
    public function setBalanceAfter($balanceAfter)
    {
        $this->balanceAfter = $balanceAfter;

        return $this;
    }

    /**
     * Get balanceAfter
     *
     * @return integer 
     */
    public function getBalanceAfter()
    {
        return $this->balanceAfter;
    }
}
