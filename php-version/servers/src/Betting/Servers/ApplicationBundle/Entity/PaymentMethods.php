<?php

namespace Betting\Servers\ApplicationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentMethods
 *
 * @ORM\Table(name="app_payment_methods")
 * @ORM\Entity(repositoryClass="Betting\Servers\ApplicationBundle\Entity\PaymentMethodsRepository")
 */
class PaymentMethods
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user", type="integer")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="globalId", type="string", length=255)
     */
    private $globalId;

    /**
     * @var string
     *
     * @ORM\Column(name="typeName", type="string", length=255)
     */
    private $typeName;

    /**
     * @var string
     *
     * @ORM\Column(name="secret", type="string", length=255)
     */
    private $secret;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param integer $user
     * @return PaymentMethods
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return integer 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set globalId
     *
     * @param string $globalId
     * @return PaymentMethods
     */
    public function setGlobalId($globalId)
    {
        $this->globalId = $globalId;

        return $this;
    }

    /**
     * Get globalId
     *
     * @return string 
     */
    public function getGlobalId()
    {
        return $this->globalId;
    }

    /**
     * Set typeName
     *
     * @param string $typeName
     * @return PaymentMethods
     */
    public function setTypeName($typeName)
    {
        $this->typeName = $typeName;

        return $this;
    }

    /**
     * Get typeName
     *
     * @return string 
     */
    public function getTypeName()
    {
        return $this->typeName;
    }

    /**
     * Set secret
     *
     * @param string $secret
     * @return PaymentMethods
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;

        return $this;
    }

    /**
     * Get secret
     *
     * @return string 
     */
    public function getSecret()
    {
        return $this->secret;
    }
}
