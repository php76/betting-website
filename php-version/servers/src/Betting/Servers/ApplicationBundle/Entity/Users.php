<?php

namespace Betting\Servers\ApplicationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Users
 *
 * @ORM\Table(name="app_users")
 * @ORM\Entity(repositoryClass="Betting\Servers\ApplicationBundle\Entity\UsersRepository")
 * @UniqueEntity(fields="email", message="Email already taken")
 * @UniqueEntity(fields="userName", message="UserName already taken")
 *
 * The following annotations tells the serializer to skip all properties which
 * have not marked with Expose.
 *
 * @ExclusionPolicy("all")
 */
class Users implements AdvancedUserInterface, \Serializable
{
    public function __construct()
    {
        $this->registrationTimeStamp = new \DateTime();
        $this->rank = 0;
        $this->userEnabled = 0;
        $this->totalBetAmount = 0;
        $this->currentBalance = 0;
        $this->secret = $this->generateHash();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255)
     * @Assert\NotBlank(groups={"registration","edit"})
     * @Expose
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255)
     * @Assert\NotBlank(groups={"registration","edit"})
     * @Expose
     */
    private $lastName;

    /**
     * @var integer
     *
     * @Assert\NotBlank(groups={"registration","edit"})
     * @ORM\ManyToOne(targetEntity="Betting\Servers\EventsBundle\Entity\Countries")
     * @ORM\JoinColumn(name="country", referencedColumnName="id", onDelete="SET NULL")
     * @Expose
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255)
     * @Assert\NotBlank(groups={"registration","edit"})
     * @Expose
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="streetAddress", type="text")
     * @Assert\NotBlank(groups={"registration","edit"})
     * @Expose
     */
    private $streetAddress;

    /**
     * @var integer
     *
     * @ORM\Column(name="zipCode", type="integer")
     * @Assert\NotBlank(groups={"registration","edit"})
     * @Expose
     */
    private $zipCode;

    /**
     * @var string
     *
     * @ORM\Column(name="userName", type="string", length=255, unique=true)
     * @Assert\NotBlank(groups={"registration","login","edit"})
     * @Expose
     */
    private $userName;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     * @Assert\NotBlank(groups={"registration","login","changePassword"})
     * @Assert\Length(min = 5,groups={"registration","login","changePassword"})
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="secret", type="string", length=255)
     */
    private $secret;

    /**
     * @var string
     *
     * @ORM\Column(name="rank", type="string", length=255)
     * @Expose
     */
    private $rank;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     * @Assert\NotBlank(groups={"registration","edit","resetPassword"})
     * @Assert\Email(groups={"registration","login","edit","resetPassword"})
     * @Expose
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthDate", type="date",nullable=true)
     * @Assert\DateTime(groups={"registration","edit"})
     * @Expose
     */
    private $birthDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="phoneNumberMain", type="integer")
     * @Assert\NotBlank(groups={"registration","edit"})
     * @Expose
     */
    private $phoneNumberMain;

    /**
     * @var integer
     *
     * @ORM\Column(name="phoneNumberAlt", type="integer")
     * @Expose
     */
    private $phoneNumberAlt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="registrationTimeStamp", type="datetime",nullable=true)
     * @Assert\DateTime()
     */
    private $registrationTimeStamp;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastLoginTimeStamp", type="datetime",nullable=true)
     * @Assert\DateTime()
     */
    private $lastLoginTimeStamp;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastActivityTimeStamp", type="datetime",nullable=true)
     * @Assert\DateTime()
     */
    private $lastActivityTimeStamp;

    /**
     * @var string
     *
     * @ORM\Column(name="lastIpAddress", type="string", length=255,nullable=true)
     */
    private $lastIpAddress;

    /**
     * @var float
     *
     * @ORM\Column(name="lastGeoLocationLon", type="float", length=255,nullable=true)
     */
    private $lastGeoLocationLon;

    /**
     * @var float
     *
     * @ORM\Column(name="lastGeoLocationLat", type="float", length=255,nullable=true)
     */
    private $lastGeoLocationLat;

    /**
     * @var boolean
     *
     * @ORM\Column(name="userEnabled", type="boolean")
     */
    private $userEnabled;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar", type="string", length=255,nullable=true)
     * @Expose
     */
    private $avatar;

    /**
     * @var string
     *
     * @ORM\Column(name="idNumber", type="string", length=255)
     * @Assert\NotBlank(groups={"registration","edit"})
     * @Expose
     */
    private $idNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="idScan", type="string", length=255,nullable=true)
     */
    private $idScan;

    /**
     * @var integer
     *
     * @ORM\Column(name="totalBetAmount", type="integer")
     */
    private $totalBetAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="currentBalance", type="integer")
     * @Expose
     */
    private $currentBalance;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Users
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Users
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set country
     *
     * @param integer $country
     * @return Users
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return integer 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Users
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set streetAddress
     *
     * @param string $streetAddress
     * @return Users
     */
    public function setStreetAddress($streetAddress)
    {
        $this->streetAddress = $streetAddress;

        return $this;
    }

    /**
     * Get streetAddress
     *
     * @return string 
     */
    public function getStreetAddress()
    {
        return $this->streetAddress;
    }

    /**
     * Set zipCode
     *
     * @param integer $zipCode
     * @return Users
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode
     *
     * @return integer 
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set userName
     *
     * @param string $userName
     * @return Users
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;

        return $this;
    }

    /**
     * Get userName
     *
     * @return string 
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Users
     */
    public function setPassword($password)
    {

        $this->password = password_hash($password, PASSWORD_BCRYPT) ;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set secret
     * @param string $secret
     * @return Users
     */
    public function setSecret($secret)
    {

        $this->secret = $secret ;

        return $this;
    }

    /**
     * Get secret
     *
     * @return string
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * Set rank
     *
     * @param string $rank
     * @return Users
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return string 
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Users
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     * @return Users
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set phoneNumberMain
     *
     * @param integer $phoneNumberMain
     * @return Users
     */
    public function setPhoneNumberMain($phoneNumberMain)
    {
        $this->phoneNumberMain = $phoneNumberMain;

        return $this;
    }

    /**
     * Get phoneNumberMain
     *
     * @return integer 
     */
    public function getPhoneNumberMain()
    {
        return $this->phoneNumberMain;
    }

    /**
     * Set phoneNumberAlt
     *
     * @param integer $phoneNumberAlt
     * @return Users
     */
    public function setPhoneNumberAlt($phoneNumberAlt)
    {
        $this->phoneNumberAlt = $phoneNumberAlt;

        return $this;
    }

    /**
     * Get phoneNumberAlt
     *
     * @return integer 
     */
    public function getPhoneNumberAlt()
    {
        return $this->phoneNumberAlt;
    }

    /**
     * Set registrationTimeStamp
     *
     * @param \DateTime $registrationTimeStamp
     * @return Users
     */
    public function setRegistrationTimeStamp($registrationTimeStamp)
    {
        $this->registrationTimeStamp = $registrationTimeStamp;

        return $this;
    }

    /**
     * Get registrationTimeStamp
     *
     * @return \DateTime 
     */
    public function getRegistrationTimeStamp()
    {
        return $this->registrationTimeStamp;
    }

    /**
     * Set lastLoginTimeStamp
     *
     * @param \DateTime $lastLoginTimeStamp
     * @return Users
     */
    public function setLastLoginTimeStamp($lastLoginTimeStamp)
    {
        $this->lastLoginTimeStamp = $lastLoginTimeStamp;

        return $this;
    }

    /**
     * Get lastLoginTimeStamp
     *
     * @return \DateTime 
     */
    public function getLastLoginTimeStamp()
    {
        return $this->lastLoginTimeStamp;
    }

    /**
     * Set lastActivityTimeStamp
     *
     * @param \DateTime $lastActivityTimeStamp
     * @return Users
     */
    public function setLastActivityTimeStamp($lastActivityTimeStamp)
    {
        $this->lastActivityTimeStamp = $lastActivityTimeStamp;

        return $this;
    }

    /**
     * Get lastActivityTimeStamp
     *
     * @return \DateTime
     */
    public function getLastActivityTimeStamp()
    {
        return $this->lastActivityTimeStamp;
    }

    /**
     * Set lastIpAddress
     *
     * @param string $lastIpAddress
     * @return Users
     */
    public function setLastIpAddress($lastIpAddress)
    {
        $this->lastIpAddress = $lastIpAddress;

        return $this;
    }

    /**
     * Get lastIpAddress
     *
     * @return string 
     */
    public function getLastIpAddress()
    {
        return $this->lastIpAddress;
    }

    /**
     * Set lastGeoLocationLon
     *
     * @param float $lastGeoLocationLon
     * @return Users
     */
    public function setLastGeoLocationLon($lastGeoLocationLon)
    {
        $this->lastGeoLocationLon = $lastGeoLocationLon;

        return $this;
    }

    /**
     * Get lastGeoLocationLon
     *
     * @return float
     */
    public function getLastGeoLocationLon()
    {
        return $this->lastGeoLocationLon;
    }

    /**
     * Set userLastGeoLocationLanLat
     *
     * @param float $lastGeoLocationLat
     * @return Users
     */
    public function setLastGeoLocationLat($lastGeoLocationLat)
    {
        $this->lastGeoLocationLat = $lastGeoLocationLat;

        return $this;
    }

    /**
     * Get lastCountry
     *
     * @return integer
     */
    public function getLastCountry()
    {
        return $this->$lastCountry;
    }

    /**
     * Set lastCountry
     *
     * @param integer  $lastCountry
     * @return Users
     */
    public function setLastCountry($lastCountry)
    {
        $this->lastCountry = $lastCountry;

        return $this;
    }

    /**
     * Get lastGeoLocationLat
     *
     * @return float
     */

    public function getLastGeoLocationLat()
    {
        return $this->lastGeoLocationLat;
    }

    /**
     * Set userEnabled
     *
     * @param boolean $userEnabled
     * @return Users
     */
    public function setUserEnabled($userEnabled)
    {
        $this->userEnabled = $userEnabled;

        return $this;
    }

    /**
     * Get userEnabled
     *
     * @return boolean 
     */
    public function getUserEnabled()
    {
        return $this->userEnabled;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     * @return Users
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string 
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set idNumber
     *
     * @param string $idNumber
     * @return Users
     */
    public function setIdNumber($idNumber)
    {
        $this->idNumber = $idNumber;

        return $this;
    }

    /**
     * Get idNumber
     *
     * @return string 
     */
    public function getIdNumber()
    {
        return $this->idNumber;
    }

    /**
     * Set idScan
     *
     * @param string $idScan
     * @return Users
     */
    public function setIdScan($idScan)
    {
        $this->idScan = $idScan;

        return $this;
    }

    /**
     * Get idScan
     *
     * @return string 
     */
    public function getIdScan()
    {
        return $this->idScan;
    }

    /**
     * Set totalBetAmount
     *
     * @param integer $totalBetAmount
     * @return Users
     */
    public function setTotalBetAmount($totalBetAmount)
    {
        $this->totalBetAmount = $totalBetAmount;

        return $this;
    }

    /**
     * Get totalBetAmount
     *
     * @return integer 
     */
    public function getTotalBetAmount()
    {
        return $this->totalBetAmount;
    }

    /**
     * Set currentBalance
     *
     * @param integer $currentBalance
     * @return Users
     */
    public function setCurrentBalance($currentBalance)
    {
        $this->currentBalance = $currentBalance;

        return $this;
    }

    /**
     * Get currentBalance
     *
     * @return integer 
     */
    public function getCurrentBalance()
    {
        return $this->currentBalance;
    }

    public function getSalt()
    {
        return null;
    }

    public function getRoles()
    {
        return array('ROLE_USER');
    }

    public function isEnabled()
    {
        return $this->userEnabled;
    }

    public function isAccountNonExpired()
    {
        return true;
    }
    public function isAccountNonLocked()
    {
        return true;
    }
    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function eraseCredentials()
    {
    }

    public function generateHash()
    {
        return base64_encode(password_hash(uniqid(), PASSWORD_BCRYPT));
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->userName,
            $this->password
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->userName,
            $this->password
            ) = unserialize($serialized);
    }

    /**
     * @return Bool Whether the user is active or not
     */
    public function isActiveNow()
    {
        // Delay during wich the user will be considered as still active
        $delay = new \DateTime('2 minutes ago');

        return ( $this->getlastActivityTimeStamp() > $delay );
    }
}
