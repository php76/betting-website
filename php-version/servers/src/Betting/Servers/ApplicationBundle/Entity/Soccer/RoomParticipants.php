<?php
/**
 * Created by IntelliJ IDEA.
 * User: workmain
 * Date: 02/10/15
 * Time: 20:20
 */

namespace Betting\Servers\ApplicationBundle\Entity\Soccer;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * RoomParticipants
 *
 * @ORM\Table(name="app_room_participants_soccer")
 * @ORM\Entity(repositoryClass="Betting\Servers\ApplicationBundle\Entity\RoomParticipantsRepository")
 * @ExclusionPolicy("all")
 */
class RoomParticipants
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Betting\Servers\ApplicationBundle\Entity\Soccer\RoomSlots", inversedBy="participants")
     * @ORM\JoinColumn(name="slot", referencedColumnName="id", onDelete="SET NULL")
     * @Expose
     */
    private $slot;

    /**
     * @var integer
     * @ORM\OneToOne(targetEntity="Betting\Servers\ApplicationBundle\Entity\Users")
     * @ORM\JoinColumn(name="user", referencedColumnName="id", onDelete="SET NULL")
     * @Expose
     */
    private $user;

    /**
     * @return int
     */
    public function getSlot()
    {
        return $this->slot;
    }

    /**
     * @param int $slot
     */
    public function setSlot($slot)
    {
        $this->slot = $slot;
    }

    /**
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param int $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }


}