<?php
/**
 * Created by IntelliJ IDEA.
 * User: workmain
 * Date: 30/09/15
 * Time: 12:20
 */

namespace Betting\Servers\ApplicationBundle\Entity\Soccer;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * RoomSlots
 *
 * @ORM\Table(name="app_room_slots_soccer")
 * @ORM\Entity(repositoryClass="Betting\Servers\ApplicationBundle\Entity\Soccer\RoomSlotsRepository")
 * @ExclusionPolicy("all")
 */
class RoomSlots
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Betting\Servers\ApplicationBundle\Entity\Soccer\Rooms", inversedBy="slots")
     * @ORM\JoinColumn(name="room", referencedColumnName="id", onDelete="SET NULL")
     * @Expose
     */
    private $room;

    /**
     * @var string
     * @ORM\Column(name="slot", type="string", length=255)
     * @Expose
     */
    private $slot;

    /**
     * @ORM\OneToMany(targetEntity="Betting\Servers\ApplicationBundle\Entity\Soccer\RoomParticipants", mappedBy="slot")
     * @Expose
     */
    private $participants;

    /**
     * @return int
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * @param int $room
     */
    public function setRoom($room)
    {
        $this->room = $room;
    }

    /**
     * @return string
     */
    public function getSlot()
    {
        return $this->slot;
    }

    /**
     * @param string $slot
     */
    public function setSlot($slot)
    {
        $this->slot = $slot;
    }

    /**
     * @return mixed
     */
    public function getParticipants()
    {
        return $this->participants;
    }

}