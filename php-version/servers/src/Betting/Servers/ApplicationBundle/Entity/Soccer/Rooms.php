<?php

namespace Betting\Servers\ApplicationBundle\Entity\Soccer;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Rooms
 *
 * @ORM\Table(name="app_rooms_soccer")
 * @ORM\Entity(repositoryClass="Betting\Servers\ApplicationBundle\Entity\Soccer\RoomsRepository")
 * @ExclusionPolicy("all")
 */
class Rooms
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string")
     * @Expose
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Betting\Servers\ApplicationBundle\Entity\Soccer\RoomTypes", inversedBy="rooms")
     * @ORM\JoinColumn(name="gameType", referencedColumnName="id", onDelete="SET NULL")
     * @Expose
     */
    private $gameType;

    /**
     * @var integer
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Betting\Servers\EventsBundle\Entity\Soccer\Events", inversedBy="rooms")
     * @ORM\JoinColumn(name="event", referencedColumnName="id", onDelete="SET NULL")
     */
    private $event;

    /**
     * @var \DateTime
     * @ORM\Column(name="openTime", type="datetime")
     * @Expose
     */
    private $openTime;

    /**
     * @var \DateTime
     * @ORM\Column(name="closeTime", type="datetime")
     * @Expose
     */
    private $closeTime;

    /**
     * @var boolean
     * @ORM\Column(name="active", type="boolean")
     * @Expose
     */
    private $active;

    /**
     * @var integer
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Betting\Servers\ApplicationBundle\Entity\Users")
     * @ORM\JoinColumn(name="admin", referencedColumnName="id", onDelete="SET NULL")
     * @Expose
     */
    private $admin;

    /**
     * @ORM\OneToMany(targetEntity="Betting\Servers\ApplicationBundle\Entity\Soccer\RoomSlots", mappedBy="room")
     * @Expose
     */
    private $slots;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getGameType()
    {
        return $this->gameType;
    }

    /**
     * @param string $gameType
     */
    public function setGameType($gameType)
    {
        $this->gameType = $gameType;
    }

    /**
     * @return int
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param int $event
     */
    public function setEvent($event)
    {
        $this->event = $event;
    }

    /**
     * @return \DateTime
     */
    public function getOpenTime()
    {
        return $this->openTime;
    }

    /**
     * @param \DateTime $openTime
     */
    public function setOpenTime($openTime)
    {
        $this->openTime = $openTime;
    }

    /**
     * @return \DateTime
     */
    public function getCloseTime()
    {
        return $this->closeTime;
    }

    /**
     * @param \DateTime $closeTime
     */
    public function setCloseTime($closeTime)
    {
        $this->closeTime = $closeTime;
    }

    /**
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getSlots()
    {
        return $this->slots;
    }

    /**
     * @param mixed $slots
     */
    public function setSlots($slots)
    {
        $this->slots = $slots;
    }

    /**
     * @return int
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * @param int $admin
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;
    }

}
