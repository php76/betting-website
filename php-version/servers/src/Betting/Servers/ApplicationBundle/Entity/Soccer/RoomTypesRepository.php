<?php

namespace Betting\Servers\ApplicationBundle\Entity\Soccer;

use Doctrine\ORM\EntityRepository;

/**
 * RoomTypesRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class RoomTypesRepository extends EntityRepository
{
}
