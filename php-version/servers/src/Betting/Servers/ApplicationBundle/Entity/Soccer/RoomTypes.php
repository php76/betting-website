<?php
/**
 * Created by IntelliJ IDEA.
 * User: workmain
 * Date: 16/11/15
 * Time: 12:20
 */

namespace Betting\Servers\ApplicationBundle\Entity\Soccer;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * RoomTypes
 *
 * @ORM\Table(name="app_room_types_soccer")
 * @ORM\Entity(repositoryClass="Betting\Servers\ApplicationBundle\Entity\Soccer\RoomTypesRepository")
 * @ExclusionPolicy("all")
 */
class RoomTypes
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     * @Expose
     */
    private $name;

    /**
     * @var integer
     * @ORM\Column(name="slotAmount", type="integer")
     * @Expose
     */
    private $slotAmount;

    /**
     * @var integer
     * @ORM\Column(name="slotAvailableAmount", type="integer")
     * @Expose
     */
    private $slotAvailableAmount;

    /**
     * @var integer
     * @ORM\Column(name="maxParticipantsPerSlot", type="integer")
     * @Expose
     */
    private $maxParticipantsPerSlot;

    /**
     * @ORM\OneToMany(targetEntity="Betting\Servers\ApplicationBundle\Entity\Soccer\Rooms", mappedBy="gameType")
     * @Expose
     */
    private $rooms;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getMaxParticipantsPerSlot()
    {
        return $this->maxParticipantsPerSlot;
    }

    /**
     * @param int $maxParticipantsPerSlot
     */
    public function setMaxParticipantsPerSlot($maxParticipantsPerSlot)
    {
        $this->maxParticipantsPerSlot = $maxParticipantsPerSlot;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * @param mixed $rooms
     */
    public function setRooms($rooms)
    {
        $this->rooms = $rooms;
    }

    /**
     * @return int
     */
    public function getSlotAmount()
    {
        return $this->slotAmount;
    }

    /**
     * @param int $slotAmount
     */
    public function setSlotAmount($slotAmount)
    {
        $this->slotAmount = $slotAmount;
    }

    /**
     * @return int
     */
    public function getSlotAvailableAmount()
    {
        return $this->slotAvailableAmount;
    }

    /**
     * @param int $slotAvailableAmount
     */
    public function setSlotAvailableAmount($slotAvailableAmount)
    {
        $this->slotAvailableAmount = $slotAvailableAmount;
    }

}