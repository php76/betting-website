<?php
/**
 * Created by IntelliJ IDEA.
 * User: cohendo
 * Date: 07/07/2015
 * Time: 17:54
 */
namespace Betting\Servers\ApplicationBundle\Listener;

use Betting\Servers\ApplicationBundle\Entity\Users;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Doctrine\ORM\EntityManager;


class UserActiveListener
{
    public function __construct(EntityManager $entityManager, SecurityContext $securityContext)
    {
        $this->securityContext = $securityContext;
        $this->entityManager = $entityManager;
    }
    public function onActive(FilterControllerEvent $event)
    {
        if ($event->getRequestType() !== HttpKernel::MASTER_REQUEST) {
            return;
        }

        // Check token authentication availability
        if ($this->securityContext->getToken()) {
            $user = $this->securityContext->getToken()->getUser();
            if ( ($user instanceof Users) && !($user->isActiveNow()) ) {
                $user->setLastActivityTimeStamp(new \DateTime());
                $this->entityManager->flush($user);
            }
        }
    }
}