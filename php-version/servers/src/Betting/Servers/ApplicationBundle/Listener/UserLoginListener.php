<?php
/**
 * Created by IntelliJ IDEA.
 * User: cohendo
 * Date: 07/07/2015
 * Time: 17:54
 */
namespace Betting\Servers\ApplicationBundle\Listener;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\SecurityContext;
use Betting\Servers\ApplicationBundle\Entity\Users;
use Doctrine\ORM\EntityManager;

class UserLoginListener
{
    public function __construct(EntityManager $entityManager, $ipapi_service,SecurityContext $securityContext)
    {
        $this->em = $entityManager;
        $this->ipapi_service = $ipapi_service;
        $this->securityContext = $securityContext;
    }
    public function onLogin(InteractiveLoginEvent $event)
    {
        try {
            $userQuery = $this->ipapi_service->byIpAddress();
        }catch (\Exception $e){
        }

        $userProfile  = $this->securityContext->getToken()->getUser();
        if ($userProfile instanceof Users){
            $userProfile->setLastLoginTimeStamp(new \DateTime());
            $userProfile->setLastGeoLocationLat('32.0114');
            $userProfile->setLastGeoLocationLon('34.7722');
            $userProfile->setLastIpAddress($userQuery['query']);
            $this->em->persist($userProfile);
            $this->em->flush();
        }
    }
}