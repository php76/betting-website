<?php
/**
 * Created by IntelliJ IDEA.
 * User: workmain
 * Date: 24/07/15
 * Time: 10:17
 */
namespace Betting\Servers\ApplicationBundle\Listener;

use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Exception\DisabledException;
use Symfony\Component\Security\Core\Exception\LockedException;

class AjaxAuthenticationListener
{

    /**
     * Handles security related exceptions.
     *
     * @param GetResponseForExceptionEvent $event An GetResponseForExceptionEvent instance
     */
    public function onCoreException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if ($exception instanceof AuthenticationException || $exception instanceof AccessDeniedException || $exception instanceof AuthenticationCredentialsNotFoundException || $exception instanceof LockedException) {
            if ($exception instanceof AuthenticationCredentialsNotFoundException){
                throw new HttpException(401, "User Credentials Not Found");
            }else if($exception instanceof DisabledException){
                throw new HttpException(401, "User Disabled");
            }else if($exception instanceof LockedException){
                throw new HttpException(401, "User Locked");
            }else{
                throw new HttpException(401, "User Not Authenticated");
            }

        }
    }
}