<?php
/**
 * Created by IntelliJ IDEA.
 * User: dor cohen
 * Date: 04/09/15
 * Time: 15:41
 */
namespace Betting\Servers\ApplicationBundle\Listener;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Betting\Servers\EventsBundle\Entity\Places;


class SearchIndexer{

    public function __construct(TokenStorageInterface  $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function postLoad (LifecycleEventArgs $args){
        $entity = $args->getEntity();
        if ($entity instanceof Places && $this->tokenStorage->getToken()) {
            $user = $this->tokenStorage->getToken()->getUser();
            $entity->setUser_longitude($user->getLastGeoLocationLon());
            $entity->setUser_latitude($user->getLastGeoLocationLat());
        }
    }

}