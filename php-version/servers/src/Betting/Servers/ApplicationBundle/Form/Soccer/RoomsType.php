<?php

namespace Betting\Servers\ApplicationBundle\Form\Soccer;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RoomsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('openTime',null, array('widget' => 'single_text','format' => 'yyyy-MM-dd H:m:s'))
            ->add('closeTime',null, array('widget' => 'single_text','format' => 'yyyy-MM-dd H:m:s'))
            ->add('active')
            ->add('gameType')
            ->add('event')
            ->add('admin')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Betting\Servers\ApplicationBundle\Entity\Soccer\Rooms',
            'csrf_protection' => false
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'betting_servers_applicationbundle_soccer_rooms';
    }
}
