<?php

namespace Betting\Servers\ApplicationBundle\Form\Account;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LoginType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('userName',null,array('label' => 'User Name:'))
            ->add('password','password',array('label' => 'Password:'))
            ->add('Login', 'submit');
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Betting\Servers\ApplicationBundle\Entity\Users',
            'csrf_protection' => false,
            'csrf_field_name' => '_token',
            'validation_groups' => array('login')
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'accountLogin';
    }
}
