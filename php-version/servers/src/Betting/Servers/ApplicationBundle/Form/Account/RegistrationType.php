<?php

namespace Betting\Servers\ApplicationBundle\Form\Account;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class RegistrationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName',null)
            ->add('lastName',null,array('label' => 'Last Name:'))
            ->add('country','entity',array('class' => 'BettingServersEventsBundle:Countries'))
            ->add('city',null)
            ->add('streetAddress','text')
            ->add('zipCode',null)
            ->add('userName',null)
            ->add('password', 'repeated', array(
                'first_name'  => 'password',
                'second_name' => 'confirm',
                'type'        => 'password',
            ))
            ->add('email',null,array('label' => 'Email:'))
            ->add('birthDate','date',array('label' => 'Birth Date:','data' => new \DateTime('now'),'years' => range(Date('Y')-100,Date('Y'))))
            ->add('phoneNumberMain',null,array('label' => 'Phone Number (Main):'))
            ->add('phoneNumberAlt',null,array('label' => 'Phone Number (Alt):'))
            ->add('idNumber',null,array('label' => 'ID number:'))
        ;
    }

   /* public function onSubmit(FormEvent $event) {
        $form = $event->getForm();
        $data = $event->getData();

        $data
            ->setUserTotalBetAmount(0)
            ->setUserCurrentBalance(0)
            ->setUserRank(0)
            ->setUserOnLine(0)
            ->setUserEnabled(0)
            ->setUserRegistrationTimeStamp(new \DateTime())
            ->setUserHash();

        $event->setData($data);

    }*/


    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Betting\Servers\ApplicationBundle\Entity\Users',
            'csrf_protection' => false,
            'csrf_field_name' => '_token',
            'validation_groups' => array('registration')
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'accountRegistration';
    }
}
