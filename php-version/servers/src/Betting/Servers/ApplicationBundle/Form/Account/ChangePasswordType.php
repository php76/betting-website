<?php

namespace Betting\Servers\ApplicationBundle\Form\Account;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ChangePasswordType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('userPassword', 'repeated', array(
                'first_name'  => 'password',
                'second_name' => 'confirm',
                'type'        => 'password',
            ))
            ->add('Change', 'submit');
    }



    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Betting\Servers\ApplicationBundle\Entity\Users',
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            'validation_groups' => array('changePassword')
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'accountChangePassword';
    }
}
