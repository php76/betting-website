<?php

namespace Betting\Servers\ApplicationBundle\Form\Account;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ResetPasswordType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('userEmail',null,array('label' => 'User Email Address:'))
            ->add('Reset', 'submit');
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Betting\Servers\ApplicationBundle\Entity\Users',
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            'validation_groups' => array('resetPassword')
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'accountResetPassword';
    }
}
