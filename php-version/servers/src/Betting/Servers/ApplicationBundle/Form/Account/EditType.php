<?php

namespace Betting\Servers\ApplicationBundle\Form\Account;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class EditType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('userFirstName',null,array('label' => 'First Name:'))
            ->add('userLastName',null,array('label' => 'Last Name:'))
            ->add('userCountryId','entity',array('class' => 'BettingServersEventsBundle:Countries','label' => 'Country Name:'))
            ->add('userCity',null,array('label' => 'City Name:'))
            ->add('userStreetAddress','text',array('label' => 'Street Name:'))
            ->add('userZipCode',null,array('label' => 'ZipCode:'))
            ->add('userName',null,array('label' => 'User Name:'))
            ->add('userEmail',null,array('label' => 'Email:'))
            ->add('userBirthDate','date',array('label' => 'Birth Date:','years' => range(Date('Y')-100,Date('Y'))))
            ->add('userPhoneNumberMain',null,array('label' => 'Phone Number (Main):'))
            ->add('userPhoneNumberAlt',null,array('label' => 'Phone Number (Alt):'))
            //->add('userAvatar','file',array('label' => 'Avatar:'))
            ->add('userIdNumber',null,array('label' => 'ID Number:'))
            ->addEventListener(FormEvents::SUBMIT, array($this, 'onSubmit'))
            //->add('userIdScan','file',array('label' => 'Id Scan:'))
        ;
        $builder->add('Update', 'submit');
    }

    public function onSubmit(FormEvent $event) {
        $form = $event->getForm();
        $data = $event->getData();

        $data->setUserHash($data->generateHash());

        $event->setData($data);

    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Betting\Servers\ApplicationBundle\Entity\Users',
            'csrf_protection' => false,
            'csrf_field_name' => '_token',
            'validation_groups' => array('edit')
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'accountEdit';
    }
}
