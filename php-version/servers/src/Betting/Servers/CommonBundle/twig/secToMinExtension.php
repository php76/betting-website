<?php
/**
 * Created by IntelliJ IDEA.
 * User: cohendo
 * Date: 12/07/2015
 * Time: 18:40
 */

namespace Betting\Servers\CommonBundle\twig;

class secToMinExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            'secToMin' => new \Twig_SimpleFilter('secToMin', array($this, 'secToMin')),
        );
    }

    public function secToMin($time)
    {
        $minutes = floor($time/60);
        $seconds = $time - $minutes*60;
        return $minutes.":".$seconds;
    }

    public function getName()
    {
        return 'secToMin_extension';
    }
}