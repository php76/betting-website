<?php

namespace Betting\Servers\CommonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BettingServersCommonBundle:Default:index.html.twig', array('name' => $name));
    }
}
