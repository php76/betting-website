<?php
/**
 * Created by IntelliJ IDEA.
 * User: cohendo
 * Date: 08/07/2015
 * Time: 15:46
 */
namespace Betting\Servers\CommonBundle\Services;

use Symfony\Component\Validator\Constraints as Assert;

class ipApiService{

    private $curl_service;
    private $validator_service;
    private $jms_serializer;

    public function __construct($curl_service,$validator_service,$jms_serializer)
    {
        $this->curl_service = $curl_service;
        $this->validator_service = $validator_service;
        $this->jms_serializer = $jms_serializer;
    }

    public function byIpAddress()
    {
        $response = $this->curl_service
            ->setUrl('http://ip-api.com/json/')
            ->setFollow("true")
            ->createCurl();
        $responseArray = (json_decode($response['content'], 'json'));
        if (!empty($responseArray['lat'])&&!empty($responseArray['lon'])) {
            return $responseArray;
        }else{
            return null;
        }

    }

}