<?php

namespace Betting\Servers\CommonBundle\Services;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Output\ConsoleOutput;

class CurlService
{
    /*Private var's*/
    private  $url;
    private  $vars;
    private  $header;
    private  $follow;
    private  $post;

    /*Setters*/
    public function setUrl($_value){
        $this->url = $_value;
        return $this;
    }
    public function setVars($_value){
        $this->vars = $_value;
        return $this;
    }
    public function setHeader($_value){
        $this->header = $_value;
        return $this;
    }
    public function setFollow($_value){
        $this->follow = $_value;
        return $this;
    }
    public function setProxy($_value){
        $this->proxy = $_value;
        return $this;
    }
    public function setMaxTries($_value){
        $this->maxTries = $_value;
        return $this;
    }
    public function setMaxTimeout($_value){
        $this->maxTimeout = $_value;
        return $this;
    }
    public function setPost($_value){
        $this->post = $_value;
        return $this;
    }

    /*Getters*/
    public function getUrl(){
        return $this->url;
    }
    public function getVars(){
        return $this->vars;
    }
    public function getHeader(){
        return $this->header;
    }
    public function getFollow(){
        return $this->follow;
    }
    public function getProxy(){
        return $this->proxy;
    }
    public function getMaxTries(){
        return $this->maxTries;
    }
    public function getMaxTimeout(){
        return $this->maxTimeout;
    }
    public function getPost(){
        return $this->post;
    }
    
    /* general functions */
    public function createCurl(){
        try{
            $output = new ConsoleOutput();
            $response = array();
            $ch  = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($ch, CURLOPT_HEADER,false);
            //curl_setopt($ch, CURLOPT_FOLLOWLOCATION,isset($_REQUEST['follow'])?true:false);
            curl_setopt($ch, CURLOPT_ENCODING,"");
            curl_setopt($ch, CURLOPT_USERAGENT,"spider123");
            curl_setopt($ch, CURLOPT_AUTOREFERER,true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,isset($this->maxTimeout)?$this->maxTimeout:2);
            curl_setopt($ch, CURLOPT_TIMEOUT,isset($this->maxTimeout)?$this->maxTimeout:2);
            curl_setopt($ch, CURLOPT_MAXCONNECTS,isset($this->maxTries)?$this->maxTries:5);
            curl_setopt($ch, CURLOPT_MAXREDIRS,5);
            curl_setopt($ch, CURLOPT_POST,isset($this->post)?$this->post:0);
            curl_setopt($ch, CURLOPT_URL, $this->url);
            if ((isset($this->vars))&&(is_array($this->vars))){
                $vars = http_build_query($this->vars, '', '&');
                if(isset($this->post)) {
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
                }else{
                    curl_setopt($ch, CURLOPT_URL, $this->url."?".$vars);
                }
            }
            else if ((isset($this->vars))&&(!is_array($this->vars))){
                curl_setopt($ch, CURLOPT_POSTFIELDS,$this->vars);
            }
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
            curl_setopt($ch, CURLOPT_VERBOSE,false);
            curl_setopt($ch, CURLOPT_COOKIESESSION, true);
            if((isset($this->proxy))&&(is_array($this->proxy))){
                shuffle($this->proxy);
                curl_setopt($ch, CURLOPT_PROXY,$this->proxy[0] );
            }
            else if((isset($this->proxy))&&(!is_array($this->proxy))){
                curl_setopt($ch, CURLOPT_PROXY, $this->proxy);
            }
            if(isset($this->header)){
                curl_setopt($ch, CURLOPT_HTTPHEADER,$this->header);
            }

            $count = 0;
            $max_tries = 5;
            $success = true;
            do {
                if ($count>0) {
                    $output->writeln("try " . $count . " times");
                    sleep(3);
                }
                if(isset($this->follow)){
                    $response['content'] = $this->curl_exec_follow($ch);
                }
                else{
                    $response['content'] = curl_exec($ch);
                }

                $count++;
                if($count >= $max_tries) {
                    $success = false;
                    break;
                }
            }
            while(curl_errno($ch));
            if($success == false) {
                throw new \Exception("Error ".curl_errno($ch)." : ".curl_error($ch));
            }

            $response['err']     = curl_errno($ch);
            $response['errmsg']  = curl_error($ch) ;
            $response['header'] = curl_getinfo($ch);

            curl_close($ch);

            return $response;
        }catch(\Exception $e) {
            $output->writeln($e->getMessage());
            $output->writeln("The exception was created on line: " . $e->getLine()." In file ".$e->getFile());
        }
    }
    
    private function preparePostFields($array) {
        $params = array();
      
        foreach ($array as $key => $value) {
            $params[] = $key . '=' . urlencode($value);
        }
      
        return implode('&', $params);
    }
    
    private function curl_exec_follow($ch, &$maxredirect = null) {
    
        $user_agent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.5)".
                      " Gecko/20041107 Firefox/1.0";
        curl_setopt($ch, CURLOPT_USERAGENT, $user_agent );
      
        $mr = $maxredirect === null ? 5 : intval($maxredirect);
      
        if (ini_get('open_basedir') == '' && ini_get('safe_mode' == 'Off')) {
        
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $mr > 0);
          curl_setopt($ch, CURLOPT_MAXREDIRS, $mr);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  
        }
        else {
      
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        
            if ($mr > 0)
            {
                $original_url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
                $newurl = $original_url;
                
                $rch = curl_copy_handle($ch);
                
                curl_setopt($rch, CURLOPT_HEADER, true);
                curl_setopt($rch, CURLOPT_NOBODY, true);
                curl_setopt($rch, CURLOPT_FORBID_REUSE, false);
                do
                {
                    curl_setopt($rch, CURLOPT_URL, $newurl);
                    $header = curl_exec($rch);
                    if (curl_errno($rch)) {
                        $code = 0;
                    }
                    else {
                        $code = curl_getinfo($rch, CURLINFO_HTTP_CODE);
                        if ($code == 301 || $code == 302) {
                            preg_match('/Location:(.*?)\n/', $header, $matches);
                            $newurl = trim(array_pop($matches));
                            if(!preg_match("/^https?:/i", $newurl)){
                                $newurl = $original_url . $newurl;
                            }   
                        }
                        else {
                            $code = 0;
                        }
                    }
                }
                while ($code && --$mr);
              
                curl_close($rch);
              
                if (!$mr)
                {
                    if ($maxredirect === null)
                        trigger_error('Too many redirects.', E_USER_WARNING);
                    else
                        $maxredirect = 0;
                    
                    return false;
                }
                curl_setopt($ch, CURLOPT_URL, $newurl);
            }
        }
        
        return curl_exec($ch);
    }
}