<?php
/**
 * Created by IntelliJ IDEA.
 * User: cohendo
 * Date: 19/05/2015
 * Time: 15:46
 */
namespace Betting\Servers\CommonBundle\Services;

use Symfony\Component\Validator\Constraints as Assert;

class GoogleService{


    private $curl_service;
    private $validator_service;
    private $jms_serializer;

    public function __construct($curl_service,$validator_service,$jms_serializer)
    {
        $this->curl_service = $curl_service;
        $this->validator_service = $validator_service;
        $this->jms_serializer = $jms_serializer;
    }



    public function getByAddress($address)
    {
        $response = $this->curl_service
            ->setUrl('http://maps.googleapis.com/maps/api/geocode/json')
            ->setVars(['address'=>urlencode($address)])
            ->setFollow("true")
            ->createCurl();
        $responseArray = (json_decode($response['content'], 'json'));
        if (!empty($responseArray['results'])) {
            foreach ($responseArray['results'][0]['address_components'] as $addressResponse) {
                if (isset($addressResponse['types'][0]) && $addressResponse['types'][0] === 'country') {
                    $placeCountry = $addressResponse['long_name'];
                }
            }
            if (isset($placeCountry)&&isset($responseArray['results'][0]['types'][0])&&isset($responseArray['results'][0]['geometry']['location'])) {
                $placeDescription = $responseArray['results'][0]['types'][0];
                return ['placeCountry' => $placeCountry, 'placeDescription' => $placeDescription, 'location' => $responseArray['results'][0]['geometry']['location'] ];
            }else{
                return null;
            }
        }else{
            return null;
        }

    }

}