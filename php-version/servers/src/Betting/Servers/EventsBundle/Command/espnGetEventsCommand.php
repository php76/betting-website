<?php

namespace Betting\Servers\EventsBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;


class espnGetEventsCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('espn:getEvents')
            ->setDescription('Get ESPN event data')
            ->addArgument(
                'resource',
                InputArgument::REQUIRED,
                'resource type name'
            )
            ->addArgument(
                'sport',
                InputArgument::REQUIRED,
                'sport type name'
            )
            ->addArgument(
                'date',
                InputArgument::REQUIRED,
                'get data from date'
            )
            ->setHelp(<<<EOF
The <info>%command.name%</info> command create a url request:

for example :

<info>php %command.full_name%</info> <comment>android.opt.ws.eurosport.com/PublicMatchFactoryOpt.asmx/FindMatches <info>--vars</info>="competitionId=-1&disciplineId=-1&eventId=-1&genderId=-1&groupId=-1&languageId=0&playerId=-1&recurringEventId=340&roundId=5189&seasonId=-1&sportId=22&teamId=-1" <info>--follow</info>="allow" <info>--header</info>="Authorization: Basic QW5kcm9pZDp3ZG1aOHoscHZcJ2FHeWViRVRpK2pTbyZu" </comment>
EOF
            );
    }


    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $espnController = $this->getContainer()->get('espn_controller');
        $espnController->getEvents($input,$output);

    }
}

