<?php

namespace Betting\Servers\EventsBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;


class curlCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('curl:execute')
            ->setDescription('create a Curl request')
            ->addArgument('-u', InputArgument::REQUIRED, 'url include get var`s' )
            ->addOption('vars',null,InputOption::VALUE_REQUIRED,'add variables',null)
            ->addOption('follow',null,InputOption::VALUE_REQUIRED,'allow follow','yes')
            ->addOption('header',null,InputOption::VALUE_REQUIRED,'add header information',null)
            ->addOption('proxy',null,InputOption::VALUE_REQUIRED,'add proxy server',null)
            ->setHelp(<<<EOF
The <info>%command.name%</info> command create a url request:

for example :

<info>php %command.full_name%</info> <comment>android.opt.ws.eurosport.com/PublicMatchFactoryOpt.asmx/FindMatches <info>--vars</info>="competitionId=-1&disciplineId=-1&eventId=-1&genderId=-1&groupId=-1&languageId=0&playerId=-1&recurringEventId=340&roundId=5189&seasonId=-1&sportId=22&teamId=-1" <info>--follow</info>="allow" <info>--header</info>="Authorization: Basic QW5kcm9pZDp3ZG1aOHoscHZcJ2FHeWViRVRpK2pTbyZu" </comment>
EOF
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        
        $url        =   $input->getArgument('-u');
        $follow     =   $input->getOption('follow');
        $header     =   array($input->getOption('header'));
        $vars       =   $input->getOption('vars');
        $proxy      =   $input->getOption('proxy');
        
        $serverResponse = $this->getContainer()->get('utils_curl')
            ->setUrl($url)
            ->setHeader($header)
            ->setFollow($follow)
            ->setVars($vars)
            ->setProxy($proxy)
            ->createCurl();

        $output->writeln(sprintf('<comment>'.print_r($serverResponse['content']).'</comment>'));
    }
}

