<?php

namespace Betting\Servers\EventsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Countries
 *
 * @ORM\Table(name="events_countries")
 * @ORM\Entity(repositoryClass="Betting\Servers\EventsBundle\Entity\CountriesRepository")
 *
 * The following annotations tells the serializer to skip all properties which
 * have not marked with Expose.
 *
 * @ExclusionPolicy("all")
 */
class Countries
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="countryCode", type="string", length=255)
     * @Expose
     */
    private $countryCode;

    /**
     * @var string
     *
     * @ORM\Column(name="nameEn", type="string", length=255)
     * @Expose
     */
    private $nameEn;

    /**
     * @var string
     *
     * @ORM\Column(name="nameFr", type="string", length=255)
     */
    private $nameFr;

    /**
     * @var string
     * @ORM\Column(name="flag", type="string", length=255)
     * @Expose
     */
    private $flag;

    /**
     * @var string
     *
     * @ORM\Column(name="languages", type="string", length=255)
     */
    private $languages;

    /**
     * @var string
     * @ORM\Column(name="borderNorth", type="float", length=255)
     */
    private $borderNorth;

    /**
     * @var string
     *
     * @ORM\Column(name="borderSouth", type="float", length=255)
     */
    private $borderSouth;

    /**
     * @var string
     *
     * @ORM\Column(name="borderEast", type="float", length=255)
     */
    private $borderEast;

    /**
     * @var float
     *
     * @ORM\Column(name="borderWest", type="float")
     */
    private $borderWest;

    /**
     * @var integer
     *
     * @ORM\Column(name="populationNumber", type="integer")
     */
    private $populationNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="cityNumber", type="integer")
     */
    private $cityNumber;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set countryCode
     *
     * @param string $countryCode
     * @return Countries
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * Get countryCode
     *
     * @return string 
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Set nameEn
     *
     * @param string $nameEn
     * @return Countries
     */
    public function setNameEn($nameEn)
    {
        $this->nameEn = $nameEn;

        return $this;
    }

    /**
     * Get nameEn
     *
     * @return string 
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    /**
     * Set nameFr
     *
     * @param string $nameFr
     * @return Countries
     */
    public function setNameFr($nameFr)
    {
        $this->nameFr = $nameFr;

        return $this;
    }

    /**
     * Get nameFr
     *
     * @return string 
     */
    public function getNameFr()
    {
        return $this->nameFr;
    }

    /**
     * Set flag
     *
     * @param string $flag
     * @return Countries
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return string 
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Set languages
     *
     * @param string $languages
     * @return Countries
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;

        return $this;
    }

    /**
     * Get languages
     *
     * @return string 
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * Set borderNorth
     *
     * @param string $borderNorth
     * @return Countries
     */
    public function setBorderNorth($borderNorth)
    {
        $this->borderNorth = $borderNorth;

        return $this;
    }

    /**
     * Get borderNorth
     *
     * @return string 
     */
    public function getBorderNorth()
    {
        return $this->borderNorth;
    }

    /**
     * Set borderSouth
     *
     * @param string $borderSouth
     * @return Countries
     */
    public function setBorderSouth($borderSouth)
    {
        $this->borderSouth = $borderSouth;

        return $this;
    }

    /**
     * Get borderSouth
     *
     * @return string 
     */
    public function getBorderSouth()
    {
        return $this->borderSouth;
    }

    /**
     * Set borderEast
     *
     * @param string $borderEast
     * @return Countries
     */
    public function setBorderEast($borderEast)
    {
        $this->borderEast = $borderEast;

        return $this;
    }

    /**
     * Get borderEast
     *
     * @return string 
     */
    public function getBorderEast()
    {
        return $this->borderEast;
    }

    /**
     * Set borderWest
     *
     * @param float $borderWest
     * @return Countries
     */
    public function setBorderWest($borderWest)
    {
        $this->borderWest = $borderWest;

        return $this;
    }

    /**
     * Get borderWest
     *
     * @return float 
     */
    public function getBorderWest()
    {
        return $this->borderWest;
    }

    /**
     * Set populationNumber
     *
     * @param integer $populationNumber
     * @return Countries
     */
    public function setPopulationNumber($populationNumber)
    {
        $this->populationNumber = $populationNumber;

        return $this;
    }

    /**
     * Get populationNumber
     *
     * @return integer 
     */
    public function getPopulationNumber()
    {
        return $this->populationNumber;
    }

    /**
     * Set cityNumber
     *
     * @param integer $cityNumber
     * @return Countries
     */
    public function setCityNumber($cityNumber)
    {
        $this->cityNumber = $cityNumber;

        return $this;
    }

    /**
     * Get cityNumber
     *
     * @return integer 
     */
    public function getCityNumber()
    {
        return $this->cityNumber;
    }

    public function __toString()
    {
        return $this->getnameEn();
    }
}
