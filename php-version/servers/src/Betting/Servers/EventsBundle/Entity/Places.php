<?php

namespace Betting\Servers\EventsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Inline;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Places
 *
 * @ORM\Table(name="events_places")
 * @ORM\Entity(repositoryClass="Betting\Servers\EventsBundle\Entity\PlacesRepository")
 * @ExclusionPolicy("all")
 */
class Places
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     * @Expose
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     * @Expose
     */
    private $description;

    /**
     * @var float
     * @ORM\Column(name="lon", type="float", nullable=true)
     * @Expose
     */
    private $lon;

    /**
     * @var float
     * @ORM\Column(name="lat", type="float", nullable=true)
     * @Expose
     */
    private $lat;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Countries")
     * @ORM\JoinColumn(name="country", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\NotBlank()
     * @Expose
     */
    private $country;


    private $user_longitude;

    public function setUser_longitude($user_longitude){
        $this->user_longitude = $user_longitude;
    }

    private $user_latitude;

    public function setUser_latitude($user_latitude){
        $this->user_latitude = $user_latitude;
    }

    /**
     * @var string
     * @VirtualProperty
     * @Type("string")
     * @SerializedName("distance")
     * @return string
     */
    public function distance(){

        $longitude2 = $this->getLon();
        $latitude2  = $this->getLat();

        $theta = $this->user_longitude - $longitude2;
        $miles = (sin(deg2rad($this->user_latitude)) * sin(deg2rad($latitude2))) + (cos(deg2rad($this->user_latitude)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
        $miles = acos($miles);
        $miles = rad2deg($miles);
        $miles = $miles * 60 * 1.1515;
        $kilometers = $miles * 1.609344;
        return round($kilometers);

    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Places
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Places
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set longitude
     *
     * @param float $lon
     * @return Places
     */
    public function setLon($lon)
    {
        $this->lon = $lon;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * Set latitude
     *
     * @param float $lat
     * @return Places
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set country
     *
     * @param integer $country
     * @return Places
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return integer 
     */
    public function getCountry()
    {
        return $this->country;
    }
}
