<?php

namespace Betting\Servers\EventsBundle\Entity\Soccer;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TeamRelation
 *
 * @ORM\Table(name="events_team_relation_soccer")
 * @ORM\Entity(repositoryClass="Betting\Servers\EventsBundle\Entity\Soccer\TeamRelationRepository")
 */
class TeamRelation
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Teams")
     * @ORM\JoinColumn(name="team", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\NotBlank()
     */
    private $team;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Events", inversedBy="teams")
     * @ORM\JoinColumn(name="event", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\NotBlank()
     */
    private $event;

    /**
     * @var integer
     *
     * @ORM\Column(name="finalScore", type="integer")
     */
    private $finalScore;
    /**
     * @var boolean
     * @ORM\Column(name="home", type="boolean")
     */
    private $home;

    /**
     * @var boolean
     * @ORM\Column(name="away", type="boolean")
     */
    private $away;

    /**
     * @var integer
     *
     * @ORM\Column(name="yellowCards", type="integer", nullable=true)
     */
    private $yellowCards;

    /**
     * @var integer
     *
     * @ORM\Column(name="fouls", type="integer", nullable=true)
     */
    private $fouls;

    /**
     * @var integer
     *
     * @ORM\Column(name="offsides", type="integer", nullable=true)
     */
    private $offsides;

    /**
     * @var integer
     *
     * @ORM\Column(name="totalShots", type="integer", nullable=true)
     */
    private $totalShots;

    /**
     * @var integer
     *
     * @ORM\Column(name="shotsOnTarget", type="integer", nullable=true)
     */
    private $shotsOnTarget;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set team
     *
     * @param integer $team
     * @return TeamRelation
     */
    public function setTeam($team)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return integer 
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Set event
     *
     * @param integer $event
     * @return TeamRelation
     */
    public function setEvent($event)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return integer
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set finalScore
     *
     * @param integer $finalScore
     * @return TeamRelation
     */
    public function setFinalScore($finalScore)
    {
        $this->finalScore = $finalScore;

        return $this;
    }

    /**
     * Get finalScore
     *
     * @return integer
     */
    public function getFinalScore()
    {
        return $this->finalScore;
    }

    /**
     * Set home
     *
     * @param boolean $home
     * @return TeamRelation
     */
    public function setHome($home)
    {
        $this->home = $home;

        return $this;
    }

    /**
     * Get home
     *
     * @return boolean
     */
    public function getHome()
    {
        return $this->home;
    }

    /**
     * Set away
     *
     * @param boolean $away
     * @return TeamRelation
     */
    public function setAway($away)
    {
        $this->away = $away;

        return $this;
    }

    /**
     * Get away
     *
     * @return boolean
     */
    public function getAway()
    {
        return $this->away;
    }

    /**
     * @return int
     */
    public function getYellowCards()
    {
        return $this->yellowCards;
    }

    /**
     * @param int $yellowCards
     */
    public function setYellowCards($yellowCards)
    {
        $this->yellowCards = $yellowCards;
    }

    /**
     * @return int
     */
    public function getFouls()
    {
        return $this->fouls;
    }

    /**
     * @param int $fouls
     */
    public function setFouls($fouls)
    {
        $this->fouls = $fouls;
    }

    /**
     * @return int
     */
    public function getOffsides()
    {
        return $this->offsides;
    }

    /**
     * @param int $offsides
     */
    public function setOffsides($offsides)
    {
        $this->offsides = $offsides;
    }

    /**
     * @return int
     */
    public function getTotalShots()
    {
        return $this->totalShots;
    }

    /**
     * @param int $totalShots
     */
    public function setTotalShots($totalShots)
    {
        $this->totalShots = $totalShots;
    }

    /**
     * @return int
     */
    public function getShotsOnTarget()
    {
        return $this->shotsOnTarget;
    }

    /**
     * @param int $shotsOnTarget
     */
    public function setShotsOnTarget($shotsOnTarget)
    {
        $this->shotsOnTarget = $shotsOnTarget;
    }
}
