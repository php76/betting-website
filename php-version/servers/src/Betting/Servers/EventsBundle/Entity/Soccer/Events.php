<?php

namespace Betting\Servers\EventsBundle\Entity\Soccer;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Inline;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Events
 *
 * @ORM\Table(name="events_upcoming_soccer")
 * @ORM\Entity(repositoryClass="Betting\Servers\EventsBundle\Entity\Soccer\EventsRepository")
 *
 * The following annotations tells the serializer to skip all properties which
 * have not marked with Expose.
 *
 * @ExclusionPolicy("all")
 */
class Events
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="EventsHighLights", mappedBy="event")
     * @Expose
     */
    private $highlights;

    /**
     * @ORM\OneToMany(targetEntity="TeamRelation", mappedBy="event")
     * @Expose
     */
    private $teams;


    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     * @Expose
     *
     */
    private $name;

    /**
     * @var \DateTime
     * @ORM\Column(name="startDate", type="datetime")
     * @Assert\NotBlank()
     * @Expose

     */
    private $startDate;

    /**
     * @var \DateTime
     * @ORM\Column(name="endDate", type="datetime")
     * @Expose
     */
    private $endDate;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Betting\Servers\EventsBundle\Entity\Places")
     * @ORM\JoinColumn(name="place", referencedColumnName="id", onDelete="SET NULL")
     * @Expose
     */
    private $place;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Leagues")
     * @ORM\JoinColumn(name="league", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\NotBlank()
     * @Expose
     */
    private $league;

    /**
     * @var string
     * @ORM\Column(name="descriptionLong", type="text")
     * @Assert\NotBlank()
     * @Expose
     */
    private $descriptionLong;

    /**
     * @var string
     * @ORM\Column(name="descriptionShort", type="text")
     * @Assert\NotBlank()
     * @Expose
     */
    private $descriptionShort;

    /**
     * @var string
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     * @Expose
     */
    private $status;

    /**
     * @var string
     * @ORM\Column(name="videoLink", type="string", length=255, nullable=true)
     * @Expose
     */
    private $videoLink;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="providerName", type="string", length=255)
     */
    private $providerName;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="providerEventId", type="string", length=255)
     */
    private $providerEventId;

    /**
     * @ORM\OneToMany(targetEntity="Betting\Servers\ApplicationBundle\Entity\Soccer\Rooms", mappedBy="event")
     * @Expose
     */
    private $rooms;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Events
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return Events
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return Events
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set place
     *
     * @param integer $place
     * @return Events
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return integer
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set league
     *
     * @param integer $league
     * @return Events
     */
    public function setLeague($league)
    {
        $this->league = $league;

        return $this;
    }

    /**
     * Get league
     *
     * @return integer
     */
    public function getLeague()
    {
        return $this->league;
    }

    /**
     * Set descriptionLong
     *
     * @param string $descriptionLong
     * @return Events
     */
    public function setDescriptionLong($descriptionLong)
    {
        $this->descriptionLong = $descriptionLong;

        return $this;
    }

    /**
     * Get descriptionLong
     *
     * @return string
     */
    public function getDescriptionLong()
    {
        return $this->descriptionLong;
    }

    /**
     * Set descriptionShort
     *
     * @param string $descriptionShort
     * @return Events
     */
    public function setDescriptionShort($descriptionShort)
    {
        $this->descriptionShort = $descriptionShort;

        return $this;
    }

    /**
     * Get descriptionShort
     *
     * @return string
     */
    public function getDescriptionShort()
    {
        return $this->descriptionShort;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Events
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set videoLink
     *
     * @param string $videoLink
     * @return Events
     */
    public function setVideoLink($videoLink)
    {
        $this->videoLink = $videoLink;

        return $this;
    }

    /**
     * Get videoLink
     *
     * @return string
     */
    public function getVideoLink()
    {
        return $this->videoLink;
    }

    /**
     * Set providerName
     *
     * @param string $providerName
     * @return Events
     */
    public function setProviderName($providerName)
    {
        $this->providerName = $providerName;

        return $this;
    }

    /**
     * Get providerName
     *
     * @return string
     */
    public function getProviderName()
    {
        return $this->providerName;
    }

    /**
     * Set providerEventId
     *
     * @param string $providerEventId
     * @return Events
     */
    public function setProviderEventId($providerEventId)
    {
        $this->providerEventId = $providerEventId;

        return $this;
    }

    /**
     * Get providerEventId
     *
     * @return string
     */
    public function getProviderEventId()
    {
        return $this->providerEventId;
    }
}
