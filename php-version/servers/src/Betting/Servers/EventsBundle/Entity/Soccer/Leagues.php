<?php

namespace Betting\Servers\EventsBundle\Entity\Soccer;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Inline;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * Leagues
 *
 * @ORM\Table(name="events_leagues_soccer")
 * @ORM\Entity(repositoryClass="Betting\Servers\EventsBundle\Entity\Soccer\LeaguesRepository")
 * @UniqueEntity("nameLong")
 * @ExclusionPolicy("all")
 */
class Leagues
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="nameLong", type="string", length=255, unique=true)
     * @Expose
     */
    private $nameLong;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="nameShort", type="string", length=255, nullable=true)
     * @Expose
     */
    private $nameShort;

    /**
     * @ORM\OneToMany(targetEntity="Events", mappedBy="league")
     * @Expose
     */
    private $events;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameLong
     *
     * @param string $nameLong
     * @return Leagues
     */
    public function setNameLong($nameLong)
    {
        $this->nameLong = $nameLong;

        return $this;
    }

    /**
     * Get nameLong
     *
     * @return string 
     */
    public function getNameLong()
    {
        return $this->nameLong;
    }

    /**
     * Set nameShort
     *
     * @param string $nameShort
     * @return Leagues
     */
    public function setNameShort($nameShort)
    {
        $this->nameShort = $nameShort;

        return $this;
    }

    /**
     * Get nameShort
     *
     * @return string 
     */
    public function getNameShort()
    {
        return $this->nameShort;
    }

    /**
     * Get events
     *
     * @return entity
     */
    public function getEvents()
    {
        return $this->events;
    }
}
