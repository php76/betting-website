<?php

namespace Betting\Servers\EventsBundle\Entity\Soccer;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * EventsHighLights
 *
 * @ORM\Table(name="events_highlights_soccer")
 * @ORM\Entity(repositoryClass="Betting\Servers\EventsBundle\Entity\Soccer\EventsHighLightsRepository")
 *
 * The following annotations tells the serializer to skip all properties which
 * have not marked with Expose.
 *
 * @ExclusionPolicy("all")
 *
 */
class EventsHighLights
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Events", inversedBy="highlights")
     * @ORM\JoinColumn(name="event", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\NotBlank()
     */
    private $event;


    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Teams")
     * @ORM\JoinColumn(name="team", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\NotBlank()
     * @Expose
     */
    private $team;

    /**
     * @var \DateTime
     * @Expose
     * @ORM\Column(name="clockTime", type="integer")
     * @Assert\NotBlank()
     */
    private $clockTime;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="descriptionShort", type="string", length=255)
     * @Expose
     */
    private $descriptionShort;

    /**
     * @var text
     * @ORM\Column(name="descriptionLong", type="text", nullable=true)
     * @Expose
     */
    private $descriptionLong;

    /**
     * @var integer
     * @ORM\Column(name="teamScore", type="integer")
     * @Expose
     */
    private $teamScore;

    /**
     * @var boolean
     * @ORM\Column(name="goal", type="boolean", nullable=true)
     * @Expose
     */
    private $goal;

    /**
     * @var boolean
     * @ORM\Column(name="redCard", type="boolean", nullable=true)
     * @Expose
     */
    private $redCard;

    /**
     * @var boolean
     * @ORM\Column(name="yellowCard", type="boolean", nullable=true)
     * @Expose
     */
    private $yellowCard;

    /**
     * @var boolean
     * @ORM\Column(name="foul", type="boolean", nullable=true)
     * @Expose
     */
    private $foul;

    /**
     * @var boolean
     * @ORM\Column(name="offSide", type="boolean", nullable=true)
     * @Expose
     */
    private $offSide;

    /**
     * @var boolean
     * @ORM\Column(name="gameStart", type="boolean", nullable=true)
     * @Expose
     */
    private $gameStart;

    /**
     * @var boolean
     * @ORM\Column(name="gameEnd", type="boolean", nullable=true)
     * @Expose
     */
    private $gameEnd;

    /**
     * @var boolean
     * @ORM\Column(name="extension", type="boolean", nullable=true)
     * @Expose
     */
    private $extension;

    /**
     * @var integer
     * @ORM\Column(name="offSideSum", type="integer", options={"default" = 0})
     * @Expose
     */
    private $offSideSum;

    /**
     * @var integer
     * @ORM\Column(name="foulSum", type="integer", options={"default" = 0})
     * @Expose
     */
    private $foulSum;

    /**
     * @var integer
     * @ORM\Column(name="redCardSum", type="integer", options={"default" = 0})
     * @Expose
     */
    private $redCardSum;

    /**
     * @var integer
     * @ORM\Column(name="yellowCardSum", type="integer", options={"default" = 0})
     * @Expose
     */
    private $yellowCardSum;

    /**
     * @VirtualProperty
     * @SerializedName("clock_time_formatted")
     *
     * @return string
     */
    public function getClockTimeFormatted()
    {
        $formattedTimeMinutes = sprintf("%02d", $this->clockTime/60);
        $formattedTimeSeconds = sprintf("%02d", $this->clockTime%60);
        $formattedTime = $formattedTimeMinutes.":".$formattedTimeSeconds;
        return $formattedTime;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set event
     *
     * @param integer $event
     * @return EventsHighLights
     */
    public function setEvent($event)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return integer 
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set team
     *
     * @param integer $team
     * @return EventsHighLights
     */
    public function setTeam($team)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return integer 
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Set clockTime
     *
     * @param integer $clockTime
     * @return EventsHighLights
     */
    public function setClockTime($clockTime)
    {
        $this->clockTime = $clockTime;

        return $this;
    }

    /**
     * Get clockTime
     *
     * @return integer
     */
    public function getClockTime()
    {
        return $this->clockTime;
    }

    /**
     * Set descriptionShort
     *
     * @param string $descriptionShort
     * @return EventsHighLights
     */
    public function setDescriptionShort($descriptionShort)
    {
        $this->descriptionShort = $descriptionShort;

        return $this;
    }

    /**
     * Get descriptionShort
     *
     * @return string 
     */
    public function getDescriptionShort()
    {
        return $this->descriptionShort;
    }

    /**
     * Set descriptionLong
     *
     * @param text $descriptionLong
     * @return EventsHighLights
     */
    public function setDescriptionLong($descriptionLong)
    {
        $this->descriptionLong = $descriptionLong;

        return $this;
    }

    /**
     * Get descriptionLong
     *
     * @return text
     */
    public function getDescriptionLong()
    {
        return $this->descriptionLong;
    }

    /**
     * Set teamScore
     *
     * @param integer $teamScore
     * @return EventsHighLights
     */
    public function setteamScore($teamScore)
    {
        $this->teamScore = $teamScore;

        return $this;
    }

    /**
     * Get teamScore
     *
     * @return integer 
     */
    public function getTeamScore()
    {
        return $this->teamScore;
    }

    /**
     * Set goal
     *
     * @param boolean $goal
     * @return EventsHighLights
     */
    public function setGoal($goal)
    {
        $this->goal = $goal;

        return $this;
    }

    /**
     * Get goal
     *
     * @return boolean 
     */
    public function getGoal()
    {
        return $this->goal;
    }

    /**
     * Set redCard
     *
     * @param boolean $redCard
     * @return EventsHighLights
     */
    public function setRedCard($redCard)
    {
        $this->redCard = $redCard;

        return $this;
    }

    /**
     * Get redCard
     *
     * @return boolean 
     */
    public function getRedCard()
    {
        return $this->redCard;
    }

    /**
     * Set yellowCard
     *
     * @param boolean $yellowCard
     * @return EventsHighLights
     */
    public function setYellowCard($yellowCard)
    {
        $this->yellowCard = $yellowCard;

        return $this;
    }

    /**
     * Get yellowCard
     *
     * @return boolean 
     */
    public function getYellowCard()
    {
        return $this->yellowCard;
    }

    /**
     * Set foul
     *
     * @param boolean $foul
     * @return EventsHighLights
     */
    public function setFoul($foul)
    {
        $this->foul = $foul;

        return $this;
    }

    /**
     * Get foul
     *
     * @return boolean 
     */
    public function getFoul()
    {
        return $this->foul;
    }

    /**
     * Set offSide
     *
     * @param boolean $offSide
     * @return EventsHighLights
     */
    public function setOffSide($offSide)
    {
        $this->offSide = $offSide;

        return $this;
    }

    /**
     * Get offSide
     *
     * @return boolean 
     */
    public function getOffSide()
    {
        return $this->offSide;
    }

    /**
     * Set gameStart
     *
     * @param boolean $gameStart
     * @return EventsHighLights
     */
    public function setGameStart($gameStart)
    {
        $this->gameStart = $gameStart;

        return $this;
    }

    /**
     * Get gameStart
     *
     * @return boolean 
     */
    public function getGameStart()
    {
        return $this->gameStart;
    }

    /**
     * Set gameEnd
     *
     * @param boolean $gameEnd
     * @return EventsHighLights
     */
    public function setGameEnd($gameEnd)
    {
        $this->gameEnd = $gameEnd;

        return $this;
    }

    /**
     * Get gameEnd
     *
     * @return boolean 
     */
    public function getGameEnd()
    {
        return $this->gameEnd;
    }

    /**
     * Set extension
     *
     * @param boolean $extension
     * @return EventsHighLights
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get extension
     *
     * @return boolean 
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set offSideSum
     *
     * @param integer $offSideSum
     * @return EventsHighLights
     */
    public function setOffSideSum($offSideSum)
    {
        $this->offSideSum = $offSideSum;

        return $this;
    }

    /**
     * Get offSideSum
     *
     * @return integer
     */
    public function getOffSideSum()
    {
        return $this->offSideSum;
    }

    /**
     * Set foulSum
     *
     * @param integer $foulSum
     * @return EventsHighLights
     */
    public function setFoulSum($foulSum)
    {
        $this->foulSum = $foulSum;

        return $this;
    }

    /**
     * Get foulSum
     *
     * @return integer
     */
    public function getFoulSum()
    {
        return $this->foulSum;
    }

    /**
     * Set redCardSum
     *
     * @param integer $redCardSum
     * @return EventsHighLights
     */
    public function setRedCardSum($redCardSum)
    {
        $this->redCardSum = $redCardSum;

        return $this;
    }

    /**
     * Get redCardSum
     *
     * @return integer
     */
    public function getRedCardSum()
    {
        return $this->redCardSum;
    }

    /**
     * Set yellowCardSum
     *
     * @param integer $yellowCardSum
     * @return EventsHighLights
     */
    public function setYellowCardSum($yellowCardSum)
    {
        $this->yellowCardSum = $yellowCardSum;

        return $this;
    }

    /**
     * Get yellowCardSum
     *
     * @return integer
     */
    public function getYellowCardSum()
    {
        return $this->yellowCardSum;
    }
}
