<?php

namespace Betting\Servers\EventsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CountriesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('countryCode')
            ->add('nameEn')
            ->add('nameFr')
            ->add('flag')
            ->add('languages')
            ->add('borderNorth')
            ->add('borderSouth')
            ->add('borderEast')
            ->add('borderWest')
            ->add('populationNumber')
            ->add('cityNumber')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Betting\Servers\EventsBundle\Entity\Countries',
            'csrf_protection' => false
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eventsCountries';
    }
}
