<?php

namespace Betting\Servers\EventsBundle\Form\Soccer;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EventsHighLightsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('clockTime')
            ->add('descriptionShort')
            ->add('descriptionLong')
            ->add('teamScore')
            ->add('goal')
            ->add('redCard')
            ->add('yellowCard')
            ->add('foul')
            ->add('offSide')
            ->add('gameStart')
            ->add('gameEnd')
            ->add('extension')
            ->add('offSideSum')
            ->add('foulSum')
            ->add('redCardSum')
            ->add('yellowCardSum')
            ->add('event')
            ->add('team')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Betting\Servers\EventsBundle\Entity\Soccer\EventsHighLights',
            'csrf_protection' => false
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eventsEventsHighLightsSoccer';
    }
}
