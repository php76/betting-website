<?php

namespace Betting\Servers\EventsBundle\Form\Soccer;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EventsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('startDate',null, array('widget' => 'single_text','format' => 'yyyy-MM-dd H:m:s'))
            ->add('endDate',null, array('widget' => 'single_text','format' => 'yyyy-MM-dd H:m:s'))
            ->add('descriptionLong')
            ->add('descriptionShort')
            ->add('status')
            ->add('videoLink')
            ->add('providerName')
            ->add('providerEventId')
            ->add('place')
            ->add('league')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Betting\Servers\EventsBundle\Entity\Soccer\Events',
            'csrf_protection' => false
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eventsEventsSoccer';
    }
}
