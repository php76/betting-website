<?php

namespace Betting\Servers\EventsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Betting\Servers\EventsBundle\Entity\Places;
use Betting\Servers\EventsBundle\Entity\Soccer\Leagues as LeaguesSoccer;
use Betting\Servers\EventsBundle\Entity\Soccer\Teams as TeamsSoccer;
use Betting\Servers\EventsBundle\Entity\Soccer\Events as EventsSoccer;
use Betting\Servers\EventsBundle\Entity\Soccer\TeamRelation as TeamRelationSoccer;
use Betting\Servers\EventsBundle\Entity\Soccer\EventsHighLights as EventsHighLightsSoccer;

use Betting\Servers\EventsBundle\Form\PlacesType;
use Betting\Servers\EventsBundle\Form\Soccer\LeaguesType as LeaguesTypeSoccer;
use Betting\Servers\EventsBundle\Form\Soccer\TeamRelationType as TeamRelationTypeSoccer;
use Betting\Servers\EventsBundle\Form\Soccer\TeamsType as TeamsTypeSoccer;
use Betting\Servers\EventsBundle\Form\Soccer\EventsType as EventsTypeSoccer;

use Betting\Servers\ApplicationBundle\Entity\Soccer\Rooms as RoomsSoccer;
use Betting\Servers\ApplicationBundle\Form\Soccer\RoomsType as RoomsTypeSoccer;

use Symfony\Component\DependencyInjection\ContainerInterface;


class espnController extends Controller
{
    private $em;
    private $formFactory;
    protected $container;

    public function __construct($em,$formFactory,ContainerInterface $container)
    {
        $this->em = $em;
        $this->formFactory = $formFactory;
        $this->container = $container;
    }

    public function getHighLights($input, $output){

        /* Get the Active events data from the database */
        $activeEvents = $this->em->createQueryBuilder()
            ->select('Events')
            ->from('Betting\Servers\EventsBundle\Entity\\' .ucfirst($input->getArgument("sport")). '\Events', 'Events')
            ->where('Events.startDate <= :now')
            ->andWhere('Events.startDate >= :today')
            ->andWhere('Events.startDate < :tomorrow')
            ->andWhere('Events.status != :fullTime')
            ->andWhere('Events.status != :postponed')
            ->andWhere('Events.providerName = :provider')
            ->setParameter('now', new \DateTime('now',new \DateTimeZone('GMT')))
            ->setParameter('today', new \DateTime('today',new \DateTimeZone('GMT')))
            ->setParameter('fullTime', "FULL TIME")
            ->setParameter('postponed', "POSTPONED")
            ->setParameter('provider', "espn")
            ->setParameter('tomorrow', new \DateTime('tomorrow',new \DateTimeZone('GMT')))
            ->getQuery()
            ->getResult();

        foreach($activeEvents as $event){
            $serverResponse = null;
            $output->writeln(sprintf('<info>Checking Highlight data for event provider id ' . $event->getProviderEventId() . ' </info>'));

            $sport = $input->getArgument('sport');
            $resource = $input->getArgument('resource');
            $apiKey = '9342q2d6jhdwvmnqueveu58q';

            $espnService = $this->container->get('espn_service');;
            $espnService
                ->setResource($resource)//ie. events
                ->setSport($sport)//ie. soccer
                ->setApikey($apiKey)
                ->setEventId($event->getProviderEventId());
            $serverResponse=$espnService->getData();

            try{

                $leagueObject = $serverResponse['sports'][0]['leagues'][0];
                $eventObject  = $leagueObject['events'][0];

                /* Update team event relation */
                foreach ($eventObject['competitions'][0]['competitors'] as $teamsObject) {
                    $team = $this->team($output, $teamsObject, $leagueObject, $sport);
                    $teamRelation = $this->teamRelation($output, $team, $event, $teamsObject);

                }

                /* Get the highlight data */
                foreach ($eventObject['competitions'][0]['details'] as $highlightObject) {
                    $clockArray = explode(":", $highlightObject['clock']);
                    $clockMinutes = (int)$clockArray[0] * 60;
                    $clockSeconds = (int)$clockArray[1];
                    $clock = $clockMinutes + $clockSeconds;

                    $addClockArray = explode(":", $highlightObject['addedClock']);
                    $addClockMinutes = (int)$addClockArray[0] * 60;
                    $addClockSeconds = (int)$addClockArray[1];
                    $addClock = $addClockMinutes + $addClockSeconds;
                    $highlight = $this->em->getRepository('Betting\Servers\EventsBundle\Entity\Soccer\EventsHighLights')->findOneBy(['clockTime'=>($clock + $addClock),'descriptionShort'=>$highlightObject['playType']['description'],'event'=>$event]);

                    if(isset($highlightObject['teamId']) && $highlightObject['teamId'] !== 0){
                        foreach ($eventObject['competitions'][0]['competitors'] as $teamsObject) {
                            if($teamsObject['team']['id'] === $highlightObject['teamId']){
                                $team = $this->team($output, $teamsObject, $leagueObject, $sport);
                            }
                        }
                    }else{
                        $team = null;
                    }

                    if (empty($highlight)) {
                        $highlight = new EventsHighLightsSoccer();
                        $highlight->setEvent($event);
                        $highlight->setClockTime($clock + $addClock);
                        $highlight->setDescriptionShort($highlightObject['playType']['description']);
                        $highlight->setDescriptionLong(isset($highlightObject['playText'])?$highlightObject['playText']:null);
                        $highlight->setRedCard($highlightObject['isRedCard']);
                        $highlight->setYellowCard($highlightObject['isYellowCard']);
                        $highlight->setTeam($team);

                        $highlight->setTeamScore($highlightObject['scoreValue']);

                        if (isset($highlightObject['scoreType']) && $highlightObject['scoreType'] === "Goal") {
                            $highlight->setGoal(true);
                        } else {
                            $highlight->setGoal(false);
                        }

                        if (isset($highlightObject['playType']) && $highlightObject['playType']['description'] === "Foul") {
                            $highlight->setFoul(true);
                        } else {
                            $highlight->setFoul(false);
                        }

                        if (isset($highlightObject['playType']) && $highlightObject['playType']['description'] === "Offside") {
                            $highlight->setOffSide(true);
                        } else {
                            $highlight->setOffSide(false);
                        }

                        if (isset($highlightObject['playType']) && $highlightObject['playType']['description'] === "End Regular Time") {
                            $highlight->setGameEnd(true);
                        } else {
                            $highlight->setGameEnd(false);
                        }

                        if (isset($highlightObject['playType']) && $highlightObject['playType']['description'] === "Kickoff") {
                            $highlight->setGameStart(true);
                        } else {
                            $highlight->setGameStart(false);
                        }

                        if ($addClock > 0) {
                            $highlight->setExtension(true);
                        } else {
                            $highlight->setExtension(false);
                        }

                        $setFoulSum = 0;
                        $setOffSideSum = 0;
                        $setRedCardSum = 0;
                        $setYellowCardSum = 0;

                        $previous_highlight = $this->em->getRepository('Betting\Servers\EventsBundle\Entity\Soccer\EventsHighLights')->findOneBy(['event'=>$event],['id' => 'DESC']);
                        if(isset($previous_highlight)) {
                            $setFoulSum       = $previous_highlight->getFoulSum() + (int)$highlight->getFoul();
                            $setOffSideSum    = $previous_highlight->getOffSideSum() + (int)$highlight->getOffSide();
                            $setRedCardSum    = $previous_highlight->getRedCardSum() + (int)$highlight->getRedCard();
                            $setYellowCardSum = $previous_highlight->getYellowCardSum() + (int)$highlight->getYellowCard();
                        }
                        $highlight->setFoulSum($setFoulSum);
                        $highlight->setOffSideSum($setOffSideSum);
                        $highlight->setRedCardSum($setRedCardSum);
                        $highlight->setYellowCardSum($setYellowCardSum);


                        $this->em->persist($highlight);
                        try{
                            $this->em->flush();
                            $output->writeln(sprintf('<comment>Flushing Highlight ' . $highlightObject['playType']['description'] . ' to database</comment>'));
                        } catch (\Exception $e) {
                            throw new \Exception($e);
                        }
                    }
                }

            }catch(\Exception $e) {
                echo $e->getMessage()."\n";
                echo "The exception was created on line: " . $e->getLine()." In file ".$e->getFile()."\n";
            }
        }
    }

    public function getEvents($input, $output)
    {

        $sport = $input->getArgument('sport');
        $resource = $input->getArgument('resource');
        $date = $input->getArgument('date');
        $apiKey = '9342q2d6jhdwvmnqueveu58q';

        $espnService = $this->container->get('espn_service');

        $espnService
            ->setResource($resource)//ie. events
            ->setSport($sport)//ie. soccer
            ->setDate($date)//ie. 20151219
            ->setApikey($apiKey);
        $serverResponse=$espnService->getData();
        /* Get the league data */
        foreach ($serverResponse['sports'][0]['leagues'] as $leagueObject) {
            $league = $this->league($output,$leagueObject);
            $country = $this->country($output,$leagueObject);
            /* Get the Events data */
            try{
                foreach ($leagueObject['events'] as $eventObject) {
                    $place = $this->place($output,$eventObject);
                    $event = $this->event($output,$eventObject,$league,$place);
                    $rooms = $this->rooms($output,$event);
                    /* Get the Teams data */
                    foreach ($eventObject['competitions'][0]['competitors'] as $teamsObject) {
                        $team = $this->team($output, $teamsObject, $leagueObject, $sport);
                        $teamRelation = $this->teamRelation($output, $team, $event, $teamsObject);

                    }
                }
            }catch(\Exception $e) {
                echo $e->getMessage()."\n";
                echo "The exception was created on line: " . $e->getLine()." In file ".$e->getFile()."\n";
            }
        }
    }

    private function league($output,$leagueObject){
        $output->writeln(sprintf('<info>Checking League ' . $leagueObject["name"] . ' </info>'));
        $league = $this->em->getRepository('Betting\Servers\EventsBundle\Entity\Soccer\Leagues')->findOneByNameLong($leagueObject['name']);
        if (empty($league)) {
            $league = new LeaguesSoccer();

            $data = array(
                'nameLong'=>$leagueObject['name'],
                'nameShort'=>$leagueObject['shortName']
            );

            $LeaguesSoccerForm = $this->formFactory->create(new LeaguesTypeSoccer(), $league);
            $LeaguesSoccerForm->submit($data);

            if ($LeaguesSoccerForm->isValid()) {
                $this->em->persist($league);
                try {
                    $this->em->flush();
                    $output->writeln(sprintf('<comment>Flushing League ' . $leagueObject["name"] . ' to database</comment>'));
                } catch (\Exception $e) {
                    throw new \Exception($e);
                }
            }else{
                throw new \Exception($LeaguesSoccerForm->getErrorsAsString());
            }
        }
        return $league;
    }

    private function place($output,$eventObject){
        $place = $this->em->getRepository('Betting\Servers\EventsBundle\Entity\Places')->findOneBy(['name' => isset($eventObject['venues'][0]['name'])?$eventObject['venues'][0]['name']:"none"]);
        if (empty($place)&&!empty($eventObject['venues'])) {
            $googleService = $this->container->get('google_service');
            $placeDescription = $googleService->getByAddress($eventObject['venues'][0]['name']);
            $country = $this->em->getRepository('Betting\Servers\EventsBundle\Entity\Countries')->findOneByNameEn(isset($placeDescription['placeCountry']) ? $placeDescription['placeCountry'] : "International");
            if(!$country){
                throw new \Exception("No such country ".$placeDescription['placeCountry']."\n");
            }
            $place = new Places();

            $data = array(
                'name'=>$eventObject['venues'][0]['name'],
                'description'=>$placeDescription['placeDescription'],
                'lon'=>$placeDescription['location']['lng'],
                'lat'=>$placeDescription['location']['lat'],
                'country'=>$country->getId()
            );

            $PlacesForm = $this->formFactory->create(new PlacesType(),$place);
            $PlacesForm->submit($data);

            if ($PlacesForm->isValid()) {
                $this->em->persist($place);
                try{
                    $this->em->flush();
                    $output->writeln(sprintf('<comment>Flushing Place ' . $eventObject['venues'][0]['name'] . ' to database</comment>'));
                } catch (\Exception $e) {
                    throw new \Exception($e);
                }
            }else{
                throw new \Exception($PlacesForm->getErrorsAsString());
            }
        }
        return $place;
    }

    private function team($output,$teamsObject,$leagueObject,$sport){
        $espnService = $this->container->get('espn_service');
        $teamData = $espnService
            ->setResource('teams')
            ->setSport($sport)
            ->setLeague($leagueObject['abbreviation'])
            ->setTeamId($teamsObject['team']['id'])
            ->setDate(null)
            ->setApiKey(null)
            ->getData();
        if(!$teamData){
            throw new \Exception("Team data connection failed\n");
        }
        $teamCountry = $this->em->getRepository('Betting\Servers\EventsBundle\Entity\Countries')->findOneByNameEn(isset($teamData['sports'][0]['leagues'][0]['country']['name'])?$teamData['sports'][0]['leagues'][0]['country']['name']:"International");
        if(!$teamCountry){
            throw new \Exception("No such country ".$teamData['sports'][0]['leagues'][0]['country']['name']."\n");
        }
        $team = $this->em->getRepository('Betting\Servers\EventsBundle\Entity\Soccer\Teams')->findOneBy(array('name' => $teamsObject['team']['name'],'country' => $teamCountry));
        if (empty($team)) {
            $team = new TeamsSoccer();

            $data = array(
                'name'=>$teamsObject['team']['name'],
                'logo'=>isset($teamData['sports'][0]['leagues'][0]['teams'][0]['logos'])?$teamData['sports'][0]['leagues'][0]['teams'][0]['logos']['full']['href']:null,
                'country'=>$teamCountry->getId()
            );

            $TeamsSoccerForm = $this->formFactory->create(new TeamsTypeSoccer(),$team);
            $TeamsSoccerForm->submit($data);

            if ($TeamsSoccerForm->isValid()) {
                $this->em->persist($team);
                try{
                    $this->em->flush();
                    $output->writeln(sprintf('<comment>Flushing Team ' . $teamsObject['team']['name'] . ' to database</comment>'));
                } catch (\Exception $e) {
                    throw new \Exception($e);
                }
            }else{
                throw new \Exception($TeamsSoccerForm->getErrorsAsString());
            }
        }
        return $team;
    }

    private function teamRelation($output,$team,$event,$teamsObject){
        $teamRelation = $this->em->getRepository('Betting\Servers\EventsBundle\Entity\Soccer\TeamRelation')->findOneBy(array('team' => $team,'event' => $event ));
        if (empty($teamRelation)) {
            $teamRelation = new TeamRelationSoccer();

            if ($teamsObject['team']['homeAway'] === 'home') {
                $home = true;
                $away = false;
            } else {
                $home = false;
                $away = true;
            }

            $data = array(
                'team'=>$team->getId(),
                'event'=>$event->getId(),
                'finalScore'=>$teamsObject['score'],
                'home'=>$home,
                'away'=>$away
            );

            $TeamRelationSoccerForm = $this->formFactory->create(new TeamRelationTypeSoccer(), $teamRelation);
            $TeamRelationSoccerForm->submit($data);

            if ($TeamRelationSoccerForm->isValid()) {
                $this->em->persist($teamRelation);
                try{
                    $this->em->flush();
                    $output->writeln(sprintf('<comment>Flushing Team ' . $team->getName() . ' To Event '. $event->getName() .' Relation to database</comment>'));
                } catch (\Exception $e) {
                    throw new \Exception($e);
                }
            }else{
                throw new \Exception($TeamRelationSoccerForm->getErrorsAsString());
            }
        }else {
            $data = array(
                'finalScore'=>isset($teamsObject['score'])?$teamsObject['score']:null,
                'yellowCards'=>isset($teamsObject['yellowCards'])?$teamsObject['yellowCards']:null,
                'offsides'=>isset($teamsObject['offsides'])?$teamsObject['offsides']:null,
                'totalShots'=>isset($teamsObject['totalShots'])?$teamsObject['totalShots']:null,
                'shotsOnTarget'=>isset($teamsObject['shotsOnTarget'])?$teamsObject['shotsOnTarget']:null,
                'fouls'=>isset($teamsObject['foulsCommitted'])?$teamsObject['foulsCommitted']:null
            );
            $TeamRelationSoccerForm = $this->formFactory->create(new TeamRelationTypeSoccer(),$teamRelation);
            $TeamRelationSoccerForm->submit($data,false);
            if ($TeamRelationSoccerForm->isValid()) {
                $this->em->persist($teamRelation);
                try{
                    $this->em->flush();
                    $output->writeln(sprintf('<comment>Updating Team ' . $team->getName() . ' To Event '. $event->getName() .' Relation to database</comment>'));
                } catch (\Exception $e) {
                    throw new \Exception($e);
                }
            }else{
                throw new \Exception($TeamRelationSoccerForm->getErrorsAsString());
            }
        }
        return $teamRelation;
    }

    private function event($output,$eventObject,$league,$place){
        $event = $this->em->getRepository('Betting\Servers\EventsBundle\Entity\Soccer\Events')->findOneBy(['name' => $eventObject['name'],'providerName' => 'espn','providerEventId' => $eventObject['id'],'startDate' => new \DateTime(date("Y-m-d H:i:s", strtotime($eventObject['date'])))]);
        if (empty($event)) {
            $event = new EventsSoccer();
            $data = array(
                'name'=>isset($eventObject['name'])?$eventObject['name']:$eventObject['type'],
                'league'=>$league->getId(),
                'startDate'=>date("Y-m-d H:i:s", strtotime($eventObject['date'])),
                'endDate'=>date("Y-m-d H:i:s", strtotime($eventObject['date'])),
                'descriptionShort'=>$eventObject['season']['description'],
                'descriptionLong'=>$eventObject['type'],
                'status'=>$eventObject['competitions'][0]['status']['description'],
                'place'=>isset($place)?$place->getId():null,
                'providerName'=>'espn',
                'providerEventId'=>$eventObject['id']
            );

            $EventsSoccerForm = $this->formFactory->create(new EventsTypeSoccer(), $event);
            $EventsSoccerForm->submit($data);

            if ($EventsSoccerForm->isValid()) {
                try{
                    $this->em->persist($event);
                    $this->em->flush();
                    $output->writeln(sprintf('<comment>Flushing Event ' . $eventObject['name'] . ' to database</comment>'));
                } catch (\Exception $e) {
                    throw new \Exception($e);
                }
            }else{
                throw new \Exception($EventsSoccerForm->getErrorsAsString());
            }
        }elseif($event->getStatus() != $eventObject['competitions'][0]['status']['description'] || $event->getPlace() != $place){

            $data = array('status'=>$eventObject['competitions'][0]['status']['description']);

            $EventsSoccerForm = $this->formFactory->create(new EventsTypeSoccer(), $event);
            $EventsSoccerForm->submit($data,false);

            if ($EventsSoccerForm->isValid()) {
                $this->em->persist($event);
                try{
                    $this->em->flush();
                    $output->writeln(sprintf('<question>Updating Event ' . $eventObject['name'] . ' to database</question>'));
                } catch (\Exception $e) {
                    throw new \Exception($e);
                }
            }else{
                throw new \Exception($EventsSoccerForm->getErrorsAsString());
            }
        }
        return $event;
    }

    private function country($output,$leagueObject){
        $country = $this->em->getRepository('Betting\Servers\EventsBundle\Entity\Countries')->findOneByNameEn(isset($leagueObject['country'])?$leagueObject['country']['name']:"International");
        return $country;
    }

    private function rooms($output,$event){

        $RoomTypes = $this->em->getRepository('Betting\Servers\ApplicationBundle\Entity\Soccer\RoomTypes')->findAll();
        foreach($RoomTypes as $RoomType){
            $Rooms = new RoomsSoccer();
            $data = array(
                'name'=>$event->getName().'('.$event->getStartDate()->format('Y-m-d').'):'.$RoomType->getName(),
                'event'=>$event->getId(),
                'openTime'=>(new \DateTime('now',new \DateTimeZone('GMT')))->format("Y-m-d H:i:s"),
                'closeTime'=>(new \DateTime('+1 week',new \DateTimeZone('GMT')))->format("Y-m-d H:i:s"),
                'gameType'=>$RoomType->getId(),
                'admin'=>1,
                'active'=>true
            );

            $RoomsSoccerForm = $this->formFactory->create(new RoomsTypeSoccer(), $Rooms);
            $RoomsSoccerForm->submit($data);

            if ($RoomsSoccerForm->isValid()) {
                $this->em->persist($Rooms);
                try{
                    $this->em->flush();
                    $output->writeln(sprintf('<comment>Flushing Room  Type' . $RoomType->getName() . ' To Event '. $event->getName() .' to database</comment>'));
                } catch (\Exception $e) {
                    throw new \Exception($e);
                }
            }else{
                throw new \Exception($RoomsSoccerForm->getErrorsAsString());
            }
        }
    }
}
