<?php
/**
 * Created by IntelliJ IDEA.
 * User: cohendo
 * Date: 19/05/2015
 * Time: 15:46
 */
namespace Betting\Servers\EventsBundle\Services;

use Symfony\Component\Process\Exception\InvalidArgumentException;
use Symfony\Component\Validator\Constraints as Assert;

class EspnService{

    private $resource;
    private $sport;
    private $date;
    private $league;
    private $apikey;
    private $eventId;
    private $athleteId;
    private $teamId;
    private $espnAPIv1 = 'http://api-app.espn.com/v1/';
    private $resourceArray = ['events','calender','teams','athletes','standings'];
    private $sportNameArray = [
        'soccer' => ['arg.1','uefa.champions','fifa.world','aut.1','bel.1','den.1','fra.1','ger.1','ned.1','ita.1','nor.1','por.1','esp.1','swe.1','sui.1','tur.1','usa.1','usa.w.1','mex.1','eng.1','eng.2','eng.3','eng.4','eng.5','eng.fa','eng.worthington','eng.trophy','eng.charity','uefa.uefa','sco.1','sco.2','sco.3','sco.4','sco.tennents','sco.cis','sco.challenge','uefa.intertoto','fifa.friendly','uefa.worldq','uefa.euroq','fifa.confederations','conmebol.libertadores','concacaf.gold','fifa.wwc','fifa.worldq','fifa.worldq.afc','fifa.worldq.caf','fifa.worldq.concacaf','fifa.worldq.conmebol','fifa.worldq.ofc','fifa.worldq.uefa','friendly','usa.open','fifa.friendly.w','fifa.olympics','fifa.olympicsq','fifa.wyc','uefa.euro','intercontinental','caf.nations','fifa.w.olympicsq','fifa.w.algarve','esp.copa_del_rey','conmebol.america','fifa.w.olympics','bra.1','chi.1','fra.2','ger.2','gre.1','ita.2','ned.2','rus.1','esp.2','wal.1','irl.1','fra.coupe_de_la_ligue','fra.coupe_de_france','esp.super_cup','nir.1','usa.misl','bra.copa_do_brazil','conmebol.sudamericana','uefa.super_cup','aus.1','col.1','uru.1','per.1','par.1','mens-college-soccer','womens-college-soccer','fifa.cwc','ecu.1','ven.1','bol.1','arg.urba','ger.dfb_pokal','ita.coppa_italia','ned.cup','rsa.1','rsa.2','jpn.1','afc.champions','afc.cup','slv.1','crc.1','afc.cupq','hon.1','gua.1','aff.championship','conmebol.sudamericano_sub20','bra.camp.carioca','bra.camp.gaucho','concacaf.champions_cup','uefa.euro_u21','fifa.world.u20','usa.world_series','concacaf.superliga','fifa.world.u17','concacaf.u23','usa.usl.1','arg.2','col.2','uefa.euro.u19','concacaf.champions','bra.2','uru.2','arg.4','par.2','ven.2','arg.5','per.2','mex.2','chi.2','arg.3','mex.interliga','uefa.europa','ecu.2','global.world_football_challenge','uefa.carling','eng.w.1','ger.super_cup','ita.super_cup','rug.irb.world','panam.m','panam.w','arg.copa','mex.copa_mx','bra.camp.paulista','usa.nwsl','ger.playoff.relegation','ned.playoff.relegation','global.champs_cup','fifa.worldq.afc.conmebol','fifa.worldq.concacaf.ofc','usa.nasl','chi.copa_chi','col.copa','caf.nations_qual','conmebol.recopa','idn.1','sgp.1','mys.1','tha.1','nga.1','arg.supercopa','mens-college-soccer','womens-college-soccer'],
        'baseball' => ['college-baseball','mlb','mens-college-basketball','nba','wnba','womens-college-basketball',],
        'football' => ['college-football','nfl'],
        'golf' => ['eur','lpga','ntw','pga','sga'],
        'hockey' => ['mens-college-hockey','womens-college-hockey','nhl',],
        'lacrosse' => ['mens-college-lacrosse', 'womens-college-lacrosse' ],
        'mma' => ['ufc','strikeforce','lfc','bfc','tfc','roc','proelite','dream','bamma','wec','pancrase','affliction','ifl','pride','sfl','sharkfights','m1','shoxc','mfc','shootojapan','shootobrazil','tpf','ringside','k1'],
        'olympics' => [],
        'racing' => ['f1','irl','nhra','sprint','truck'],
        'softball' => ['college-softball'],
        'tennis' => ['atp','wta'],
        'volleyball' => ['mens-college-volleyball','womens-college-volleyball',],
        'water-polo' => ['mens-college-water-polo','womens-college-water-polo']
    ];
    private $curl_service;
    private $validator_service;
    private $jms_serializer;

    public function __construct($curl_service,$validator_service,$jms_serializer)
    {
        $this->curl_service = $curl_service;
        $this->validator_service = $validator_service;
        $this->jms_serializer = $jms_serializer;
    }




    /*Getters*/
    public function getResource()
    {
        return $this->resource;
    }
    public function getEventId()
    {
        return $this->eventId;
    }
    public function getApikey(){
        return $this->apikey;
    }

    public function getDate(){
        return $this->date;
    }

    public function getLeague(){
        return $this->league;
    }

    public function getSport(){
        return $this->sport;
    }
    public function getAthleteId()
    {
        return $this->athleteId;
    }
    public function getTeamId()
    {
        return $this->teamId;
    }


    /*Setters*/
    public function setResource($resource)
    {
        if(in_array($resource,$this->resourceArray)) {
            $this->resource = $resource;
        }else{
            $this->invalidArgument('Resource name',$resource,(implode(',',($this->resourceArray))));
        }
        return $this;
    }
    public function setEventId($eventId)
    {
        $this->eventId = $eventId;
        return $this;
    }
    public function setApikey($apikey){
        $this->apikey = $apikey;
        return $this;
    }

    public function setDate($date){
        $this->date = $date;
        return $this;
    }

    public function setLeague($league){
        if(isset($this->sport)){
            if(in_array($league,$this->sportNameArray[$this->sport])) {
                $this->league = $league;
            }else{
                $this->invalidArgument('League name',$league,(implode(',',($this->sportNameArray[$this->sport]))));
            }
        }else{
            $this->invalidArgument('Sport name',$this->sport,(implode(',',array_keys($this->sportNameArray))));
        }
        return $this;
    }

    public function setSport($sport){
        if(array_key_exists($sport,$this->sportNameArray)) {
            $this->sport = $sport;
        }else {
            $this->invalidArgument('Sport name',$sport,(implode(',',array_keys($this->sportNameArray))));
        }
        return $this;
    }
    public function setAthleteId($athleteId)
    {
        $this->athleteId = $athleteId;
        return $this;
    }
    public function setTeamId($teamId)
    {
        $this->teamId = $teamId;
        return $this;
    }



    public function getData(){

        $this->mandatoryArguments(['resource'=>$this->resource,'sport'=>$this->sport]);

        $serviceName = implode('/',array_filter(['sports',$this->sport,$this->league,$this->resource,$this->eventId,$this->teamId,$this->athleteId]));
        $response = $this->curl_service
            ->setUrl($this->espnAPIv1.$serviceName)
            ->setVars(['dates'=>$this->date,'apikey'=>$this->apikey])
            ->setFollow("true")
            ->createCurl();

        $events = (json_decode($response['content'],'json'));
        return $events;

    }

    private function invalidArgument($name,$argument,$validString){
       throw new InvalidArgumentException($name.' '.$argument.' is not valid.'.$name.' can only be: '.$validString);
    }

    private function mandatoryArguments($fields){
        foreach($fields as $key=>$field){
            if(empty($field)) {
                throw new InvalidArgumentException('Missing Mandatory Argument : '.$key);
            }
        }
    }

}