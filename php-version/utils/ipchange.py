def send_email(user, pwd, recipient, subject, body):
    import smtplib

    gmail_user = user
    gmail_pwd = pwd
    FROM = user
    TO = recipient if type(recipient) is list else [recipient]
    SUBJECT = subject
    TEXT = body

    # Prepare actual message
    message = """\From: %s\nTo: %s\nSubject: %s\n\n%s
    """ % (FROM, ", ".join(TO), SUBJECT, TEXT)
    try:
        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.set_debuglevel(1)
        server.ehlo()
        server.starttls()
        server.ehlo()
        server.login(gmail_user, gmail_pwd)
        server.sendmail(FROM, TO, message)
        server.close()
        print 'successfully sent the mail'
    except Exception, msg:
                print msg
                print "failed to send mail"

from json import load
from urllib2 import urlopen,URLError
import time
import socket
previous_ip = None
while True:
    try:
        json_object = urlopen('http://jsonip.com',timeout=1)
        current_ip = load(json_object)['ip']
        if previous_ip!=current_ip:
            print "Your new ip address is "+current_ip
            send_email("doryan2015corp@gmail.com","a64646455",["dorc.tech@gmail.com"],"IP address has changed","Your new ip address is "+current_ip)
            previous_ip=current_ip
        else:
            print "Your ip address has not changed "+previous_ip
    except URLError as e:
        print e.reason
    except ValueError, e:
        print e
    except socket.timeout, e:
        print "Timeout error :  %r" % e
    time.sleep(60)