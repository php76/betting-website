#!/usr/bin/python

import sys
import time
import logging
from subprocess import call, Popen, PIPE, STDOUT
import threading
from datetime import datetime, timedelta
from optparse import OptionParser
import os

#SET INTERVAL FUNCTION
def setInterval(interval):
    def decorator(function):
        def wrapper(*args, **kwargs):
            stopped = threading.Event()

            def loop(): # executed in another thread
                while not stopped.wait(interval): # until stopped
                    function(*args, **kwargs)

            t = threading.Thread(target=loop)
            t.daemon = True # stop if the program exits
            t.start()
            return stopped
        return wrapper
    return decorator

#SLEEP TIME
SLEEP_HIGHLIGHTS = 20
SLEEP_EVENTS = 1800 #(30Min)

#COLORS
HEADER = '\033[95m'
OKBLUE = '\033[94m'
OKGREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'
ENDC = '\033[0m'
BOLD = '\033[1m'
UNDERLINE = '\033[4m'

#PARSER
parser = OptionParser()
parser.add_option("-p", "--path", default=".",help="ENTER SYMFONY SERVER PATH")
parser.add_option("-l", "--logs", default=".",help="ENTER LOGS PATH")
(options, args) = parser.parse_args(sys.argv)

#GLOBAL VARIABLES
logger = None
current_file_name = None
SYMFONY_PATH = options.path
LOGS_PATH = options.logs

@setInterval(10)
def DEFINE_LOG():
	#LOGGING
	global logger
	global current_file_name
	global LOGS_PATH
        os.chdir(LOGS_PATH)
        filename = 'DATA_'+datetime.now().strftime("%Y%m%d")+'.log'

        if filename != current_file_name:
            #print "creating a new log file"
            if logger is None:
                logger = logging.getLogger()
                logger.setLevel(logging.INFO)
            else:
                for handler in logger.handlers[:]:  # make a copy of the list
                    logger.removeHandler(handler)

            current_file_name = filename
            formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
            handler = logging.FileHandler(filename)
            handler.setLevel(logging.INFO)
            handler.setFormatter(formatter)
            logger.addHandler(handler)

@setInterval(1800)
def GET_EVENTS():
	global SYMFONY_PATH

        START_DATE = datetime.now()-timedelta(days=1)
        END_DATE = datetime.now()+timedelta(days=10)
        logger.info("(EVENTS) STARTING EVENTS CHECK")
        while START_DATE < END_DATE:
            EVENTS = Popen("php app/console espn:getEvents events soccer "+START_DATE.strftime("%Y%m%d"),shell=True, cwd=SYMFONY_PATH, stdout=PIPE)
            for line in iter(lambda: EVENTS.stdout.readline(), ''):
                #print "(EVENTS "+START_DATE.strftime("%Y-%m-%d")+") "+line
                logger.info("(EVENTS "+START_DATE.strftime("%Y-%m-%d")+") "+line)
            START_DATE = START_DATE + timedelta(days=+1)

@setInterval(60)
def GET_TODAY_EVENTS():
    global SYMFONY_PATH

    START_DATE = datetime.now()
    logger.info("(EVENTS) STARTING TODAY EVENTS CHECK")
    EVENTS = Popen("php app/console espn:getEvents events soccer "+START_DATE.strftime("%Y%m%d"),shell=True, cwd=SYMFONY_PATH, stdout=PIPE)
    for line in iter(lambda: EVENTS.stdout.readline(), ''):
        #print "(EVENTS "+START_DATE.strftime("%Y-%m-%d")+") "+line
        logger.info("(TODAY EVENTS "+START_DATE.strftime("%Y-%m-%d")+") "+line)

@setInterval(30)
def GET_HIGHLIGHTS():
	global SYMFONY_PATH

        logger.info("(HIGHLIGHTS) STARTING HIGHLIGHT CHECK")
        HIGHLIGHTS = Popen("php app/console espn:getHighlights events soccer",shell=True, cwd=SYMFONY_PATH, stdout=PIPE)
        for line in iter(lambda: HIGHLIGHTS.stdout.readline(), ''):
            #print "(HIGHLIGHTS") "+line
            logger.info("(HIGHLIGHTS)"+line)
        RUNCOUNTER+= 1

def main():

    stop1 = DEFINE_LOG()
    stop2 = GET_HIGHLIGHTS()
    stop3 = GET_EVENTS()
    stop4 = GET_TODAY_EVENTS()

    while True:
        time.sleep(3600)

main()