#!/bin/bash
# Betting server start

echo "CD to the working directory"
cd /opt/betting/client

echo "Update local repository"
git reset --hard
git pull

echo "Grunt build"
grunt build

echo "Start the Grunt/Yeoman build-in server"
grunt serve