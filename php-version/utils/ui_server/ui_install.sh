#!/bin/bash
# Betting server install

echo "CD to the working directory"
cd /opt/

echo "GIT clone the repository"
HOST_IP=$(ip route show | awk '/default/ {print $3}')
git init betting
cd betting
git remote add origin http://$GITLAB_USER:$GITLAB_PASS@$HOST_IP:10080/root/betting.git
git config core.sparsecheckout true
echo "client" >> .git/info/sparse-checkout
git pull origin master

echo "CD to the working directory"
cd /opt/betting/client

echo "Change deployment directory permission"
chmod -R 755 /opt/betting/client

sudo npm cache clean -f
sudo npm install -g n
sudo n stable

echo "Install npm dependencies"
#npm cache clean
sudo npm install

echo "Install Bower"
npm install -g bower

echo "Install grunt cli"
npm install -g grunt-cli

echo "Creating symbolic link for bower"
ln -s /usr/bin/nodejs /usr/bin/node

echo "Bower build with root permissions"
bower install --allow-root



