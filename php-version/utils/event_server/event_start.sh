#!/bin/bash
# Betting server start

echo "CD to the working directory"
cd /opt/betting/servers

echo "Update local repository"
git reset --hard
git pull

echo "Change deployment directory permission"
chmod -R 755 /opt/betting/servers

echo "Clear Cache And Change To Production Environment"
php app/console cache:clear --env=prod  --no-debug
php app/console assetic:dump --env=prod --no-debug

echo "Start the getPerdiodicData service"
service getPeriodicDataService start
