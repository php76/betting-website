#!/bin/bash
# Betting server install

echo "CD to the working directory"
cd /opt/

echo "GIT clone the repository"
HOST_IP=$(ip route show | awk '/default/ {print $3}')
git init betting
cd betting
git remote add origin http://$GITLAB_USER:$GITLAB_PASS@$HOST_IP:10080/root/betting.git
git config core.sparsecheckout true
echo "servers" >> .git/info/sparse-checkout
git pull origin master

echo "CD to the working directory"
cd /opt/betting/servers

echo "Change deployment directory permission"
chmod -R 755 /opt/betting/servers

echo "Download composer remote server"
curl -sS https://getcomposer.org/installer | php

echo "Install composer remote server"
sudo php composer.phar install --no-ansi --no-dev --no-interaction --no-progress --no-scripts --optimize-autoloader

echo "Update composer remote server"
sudo php composer.phar update --no-interaction

echo "Create getPeriodicData service"
update-rc.d getPeriodicDataService defaults
