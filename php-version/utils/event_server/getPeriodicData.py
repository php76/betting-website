#!/usr/bin/python

import sys
import time
import logging
from subprocess import call, Popen, PIPE, STDOUT
import threading
from datetime import datetime, timedelta
from optparse import OptionParser
import os
import multiprocessing


#SLEEP TIME
SLEEP_HIGHLIGHTS = 20
SLEEP_EVENTS = 1800 #(30Min)
SLEEP_TODAY_EVENTS = 1800

#COLORS
HEADER = '\033[95m'
OKBLUE = '\033[94m'
OKGREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'
ENDC = '\033[0m'
BOLD = '\033[1m'
UNDERLINE = '\033[4m'

#PARSER
parser = OptionParser()
parser.add_option("-p", "--path", default=".",help="ENTER SYMFONY SERVER PATH")
(options, args) = parser.parse_args(sys.argv)

#GLOBAL VARIABLES
SYMFONY_PATH = options.path


def GET_EVENTS():
  while True:
    global SYMFONY_PATH
    global SLEEP_EVENTS

    START_DATE = datetime.now()-timedelta(days=1)
    END_DATE = datetime.now()+timedelta(days=10)
    print("(EVENTS) STARTING EVENTS CHECK")
    while START_DATE < END_DATE:
        EVENTS = Popen("php app/console espn:getEvents events soccer "+START_DATE.strftime("%Y%m%d"),shell=True, cwd=SYMFONY_PATH, stdout=PIPE)
        for line in iter(lambda: EVENTS.stdout.readline(), ''):
          print "(EVENTS "+START_DATE.strftime("%Y-%m-%d")+") "+line
        START_DATE = START_DATE + timedelta(days=+1)
    time.sleep(SLEEP_EVENTS)

def GET_TODAY_EVENTS():
  while True:
    global SYMFONY_PATH
    global SLEEP_TODAY_EVENTS

    START_DATE = datetime.now()
    print("(EVENTS) STARTING TODAY EVENTS CHECK")
    EVENTS = Popen("php app/console espn:getEvents events soccer "+START_DATE.strftime("%Y%m%d"),shell=True, cwd=SYMFONY_PATH, stdout=PIPE)
    for line in iter(lambda: EVENTS.stdout.readline(), ''):
      print "(EVENTS "+START_DATE.strftime("%Y-%m-%d")+") "+line
    time.sleep(SLEEP_TODAY_EVENTS)

def GET_HIGHLIGHTS():
  while True:
    global SYMFONY_PATH
    global SLEEP_HIGHLIGHTS

    print("(HIGHLIGHTS) STARTING HIGHLIGHT CHECK")
    HIGHLIGHTS = Popen("php app/console espn:getHighlights events soccer",shell=True, cwd=SYMFONY_PATH, stdout=PIPE)
    for line in iter(lambda: HIGHLIGHTS.stdout.readline(), ''):
      print "(HIGHLIGHTS) "+line
    time.sleep(SLEEP_HIGHLIGHTS)

def main():
  h = multiprocessing.Process(target=GET_HIGHLIGHTS)
  e = multiprocessing.Process(target=GET_EVENTS)
  t = multiprocessing.Process(target=GET_TODAY_EVENTS)
  h.start()
  e.start()
  t.start()
main()