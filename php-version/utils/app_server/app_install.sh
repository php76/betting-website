#!/bin/bash
# Betting server install

echo "CD to the working directory"
cd /opt/

echo "GIT clone the repository"
HOST_IP=$(ip route show | awk '/default/ {print $3}')
git clone http://$GITLAB_USER:$GITLAB_PASS@$HOST_IP:10080/root/betting.git

echo "CD to the working directory"
cd /opt/betting/servers

echo "Change deployment directory permission"
chmod -R 755 /opt/betting/servers

echo "Download composer remote server"
curl -sS https://getcomposer.org/installer | php

echo "Install composer remote server"
sudo php composer.phar install --no-dev --no-interaction --no-ansi --no-progress
