#!/bin/bash
# Betting server start

echo "CD to the working directory"
cd /opt/betting/servers

echo "Update local repository"
git reset --hard
git pull

echo "Change deployment directory permission"
chmod -R 755 /opt/betting/servers

echo "Clear Cache And Change To Production Environment"
php app/console cache:clear --env=prod  --no-debug
php app/console assetic:dump --env=prod --no-debug

echo "Update composer remote server"
php composer.phar update --no-interaction

echo "Validate Doctrine schema"
php app/console doctrine:schema:validate

echo "Drop Doctrine schema"
#php app/console doctrine:schema:drop --force --full-database

echo "Create Doctrine schema"
#php app/console doctrine:schema:create

echo "Pre populate database countries and username"
#mysql -u betting -pdoryan646455 -h mysql betting < pre_populate.sql

echo "Change permission for cache and log directories"
chown -R www-data:www-data app/cache app/logs

echo "Creating symbolic link for NGINX"
sudo ln -s /etc/nginx/sites-available/betting /etc/nginx/sites-enabled/betting

echo "Check the nginx conf is ok"
sudo nginx -t

echo "Starting php5-fpm"
service php5-fpm start

echo "Starting NGINX"
service nginx start