# -*- coding: utf-8 -*-
from django.forms import ModelForm, Textarea, FileInput, TextInput
from games.models import Team
from django.utils.translation import gettext_lazy as _

class TeamForm(ModelForm):
  class Meta:
    model = Team
    fields = ('name', 'image', 'color', 'country')
    labels = {
        'name': _('Team Name'),
    }
    help_texts = {
        'name': _('Write Team name here'),
    }
    error_messages = {
        'name': {
            'max_length': _("This Team name is too long."),
        },
    }
    widgets = {
        'name': TextInput(attrs={'maxlength': 12}),
        'image': TextInput(attrs={}),
        'color': TextInput(attrs={}),
        'country': TextInput(attrs={}),
    }
