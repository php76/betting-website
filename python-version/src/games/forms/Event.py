# -*- coding: utf-8 -*-
from django.forms import ModelForm, Textarea, FileInput, TextInput, DateTimeInput
from games.models import Event
from django.utils.translation import gettext_lazy as _
from datetime import datetime

class EventForm(ModelForm):
  class Meta:
    model = Event
    fields = ('name', 'vendor', 'vendor_event_id', 'location', 'date', 'period', 'description', 'league')
    labels = {
        'name': _('Event Name'),
    }
    help_texts = {
        'name': _('Write Event name here'),
    }
    error_messages = {
        'name': {
            'max_length': _("This Event name is too long."),
        },
    }
    widgets = {
        'name': TextInput(attrs={'maxlength': 12}),
        'date': DateTimeInput(attrs={'value': datetime.now().strftime("%Y-%m-%d %H:%M:%S")}) #initial=
    }
