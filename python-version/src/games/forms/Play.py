# -*- coding: utf-8 -*-
from django.forms import ModelForm, Textarea, FileInput, TextInput
from games.models import Play
from django.utils.translation import gettext_lazy as _

class PlayForm(ModelForm):
  class Meta:
    model = Play
    fields = ('event', 'competitor', 'agg_score', 'status', 'clock', 'description',)
    #fields = ('created_at', 'event', 'competitor', 'agg_score', 'status', 'clock', 'description',)
    labels = {
        'description': _('Play description'),
    }
    help_texts = {
        'description': _('Write Play description here'),
    }
    error_messages = {
        'description': {
            'max_length': _("This Play description is too long."),
        },
    }
    widgets = {
        'description': TextInput(attrs={'maxlength': 12}),
    }