# -*- coding: utf-8 -*-
from django.forms import ModelForm, Textarea, FileInput, TextInput
from games.models import League
from django.utils.translation import gettext_lazy as _

class LeagueForm(ModelForm):
  class Meta:
    model = League
    fields = ('name', 'image')
    labels = {
        'name': _('League Name'),
    }
    help_texts = {
        'name': _('Write League name here'),
    }
    error_messages = {
        'name': {
            'max_length': _("This League name is too long."),
        },
    }
    widgets = {
        'name': TextInput(attrs={'maxlength': 12}),
        'image': TextInput(attrs={}),
    }