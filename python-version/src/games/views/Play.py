# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse
from games import forms, models

def playAdd(request):
    if request.method == 'POST':
        form = forms.PlayForm(request.POST)
        if form.is_valid():
            form.save()
    else:
        form = forms.PlayForm()

    html = render(request,'add_form.html',{'form': form})
    return HttpResponse(html)

def playView(request):
    results = models.Play.listAll()
    html = render(request,'view_results.html',{'results': results})
    return HttpResponse(html)