# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse
from games import forms, models

def eventAdd(request):
    if request.method == 'POST':
        form = forms.EventForm(request.POST)
        if form.is_valid():
            form.save()
    else:
        form = forms.EventForm()

    html = render(request,'add_form.html',{'form': form})
    return HttpResponse(html)

def eventView(request):
    results = models.Event.listAll()
    html = render(request,'view_results.html',{'results': results})
    return HttpResponse(html)