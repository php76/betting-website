# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Team(models.Model):
    name = models.CharField(max_length=200)
    image = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    country = models.CharField(max_length=200)

    def __str__(self):
        return self.name

    @classmethod
    def listAll(self):
        return self.objects.all()