# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from games.models import League

class Event(models.Model):
    name = models.CharField(max_length=200)
    vendor = models.CharField(max_length=2, choices=(('es', 'Espn'),), default='es')
    vendor_event_id = models.CharField(max_length=200)
    location = models.CharField(max_length=200)
    date = models.DateTimeField('event date')
    period = models.CharField(max_length=200)
    description = models.CharField(max_length=200,blank=True, null=True)
    league = models.ForeignKey(League , on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    @classmethod
    def listAll(self):
        return self.objects.all()