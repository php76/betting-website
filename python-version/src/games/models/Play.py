# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from games.models import Event, Competitor

class Play(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    competitor = models.ForeignKey(Competitor, on_delete=models.CASCADE)
    agg_score = models.IntegerField(default=0)
    status =  models.CharField(max_length=2, choices=(('pr', 'Pre'),('st', 'Start'),('nd', 'End'),), default='pr')
    clock = models.TimeField(blank=True, null=True)
    description = models.CharField(max_length=200,blank=True, null=True)

    def __str__(self):
        return self.description

    @classmethod
    def listAll(self):
        return self.objects.all()