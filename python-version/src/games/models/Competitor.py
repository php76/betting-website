# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from games.models import Event, Team
 
class Competitor(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    homeAway = models.CharField(max_length=2, choices=(('hm', 'Home'),('wy', 'Away'),), default='hm')
    winner = models.BooleanField(default=False)
    score = models.IntegerField(default=0)

    def __str__(self):
        return "%s/%s" % (self.event.name,self.team.name,)

    @classmethod
    def listAll(self):
        return self.objects.all()