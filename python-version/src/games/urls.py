from django.urls import path

from . import views

urlpatterns = [
    path('league/add/', views.leagueAdd, name='leagueAdd'),
    path('league/view/', views.leagueView, name='leagueView'),
    path('play/add/', views.playAdd, name='playAdd'),
    path('play/view/', views.playView, name='playView'),
    path('team/add/', views.teamAdd, name='teamAdd'),
    path('team/view/', views.teamView, name='teamView'),
    path('event/add/', views.eventAdd, name='eventAdd'),
    path('event/view/', views.eventView, name='eventView'),
]