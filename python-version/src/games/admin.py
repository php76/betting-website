# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import League, Event, Competitor, Play, Team

# Register your models here.
admin.site.register(League)
admin.site.register(Event)
admin.site.register(Competitor)
admin.site.register(Play)
admin.site.register(Team)
