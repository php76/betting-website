### installtion steps
#### Virtualenv
 1. Install pip 
    ```bash
    sudo apt-get update
    sudo apt-get install pip 
    ```
 2. Install virtualenv 
    ```bash
    sudo apt-get install virtualenv 
    ```
 3. Create new new virtual environment  
    ```bash
    virtualenv <directroy-name> 
    ```   
 4. Activate the new environment
    ```bash
    source bin/activate
    ```
 5. Deactivate the environment using
    ```bash
    deactivate
    ```

[virtualenv installation documentation](https://virtualenv.pypa.io/en/stable/userguide/#usage)

#### Django
Install django using 
```bash
pip install Django
```

Upgrade django using
```bash
pip install --upgrade django
```

Check version using
```bash
python3 -m django --version
```

Install psycopg2 using
```bash
pip install psycopg2
```

[django installation documentation](https://docs.djangoproject.com/en/2.1/topics/install/)

#### Docker
Installing the Docker client on Windows Subsystem for Linux
 1. Update the apt package index
 ```bash
 sudo apt-get update
 ```
 2. Install packages to allow apt to use a repository over HTTPS
 ```bash
 sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
 ```
 3. Add Docker’s official GPG key
 ```bash
 curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
 ```
 4. Verify fingerprint
 ```bash
 sudo apt-key fingerprint 0EBFCD88
 ```
 5. Set up the stable repository
 ```bash
 sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
 ```
 If had "No module named 'apt_pkg'" error do
 ```bash
 sudo vim /usr/bin/add-apt-repository
#Change package header from `python3` to `python3.5`
 ```
 6. Update the apt package index again
 ```bash
 sudo apt-get update
 ```
 7. Install Docker CE
 ```bash
 sudo apt-get install docker-ce
 ```

 [Install Docker for Windows](https://store.docker.com/editions/community/docker-ce-desktop-windows)

 [docker installation documentation](https://docs.docker.com/install/linux/docker-ce/ubuntu/#extra-steps-for-aufs)

 [Docker client on Windows Subsystem installation documentation](https://medium.com/@sebagomez/installing-the-docker-client-on-ubuntus-windows-subsystem-for-linux-612b392a44c4)

  #### MongoDB docker container
 Run the container:
 ```bash
 docker run --name mongodb -e MONGO_INITDB_ROOT_USERNAME=mongo -e MONGO_INITDB_ROOT_PASSWORD=aA646455 -p 27017:27017 -d mongo
 ```

 Remove the container:
 ```bash
 docker rm -f mongodb
 ```

 Connect to mongo shell:
 ```bash
 docker exec -it mongodb bash
 mongo --username mongo --password aA646455 --authenticationDatabase admin
 ```

 #### Postgres docker container
 Run the container:
 ```bash
 docker run --name postgres -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=aA646455 -e POSTGRES_DB=postgres -p 5432:5432 -d postgres
 ```

 Remove the container:
 ```bash
 docker rm -f postgres
 ```

 Show all postgres databases:
 ```sql
 SELECT datname FROM pg_database
 WHERE datistemplate = false;
 ```

 Show all postgres tables:
 ```sql
 SELECT table_schema,table_name
 FROM information_schema.tables
 ORDER BY table_schema,table_name;
 ```

 #### Windows development environment
  1. Install [Python 3 for windows](https://www.python.org/downloads/)
  2. Upgrade PIP
  ```powershell
  python -m pip install --upgrade pip
  ```
  3. Install virtaulenv
  ```powershell
  pip install virtualenv
  pip install virtualenvwrapper-win
  ```
  4. Make virtual environment
  ```powershell
  mkvirtualenv <environment name>
  ```
  5. Run activate script 
  ```powershell
  <path-for-env>\Scripts\activate
  ```
  6. Set Project Directory to current working directory
  ```powershell
  setprojectdir .
  ```

  [Windows virtualenv install](http://timmyreilly.azurewebsites.net/python-pip-virtualenv-installation-on-windows/)

  #### PIP requirements
  Install the PIP requirements file using:
  ```powershell
  pip install -r requirements.txt
  ```